#version 330      
 
const int MAX_POINT_LIGHTS = 10;

in vec2 TexCoord0;
in vec3 Normal0;  
in vec3 WorldPos0;
in float texType;
 
out vec4 fFragColor;      
 
struct BaseLight  
{
    vec3 Color;   
    float AmbientIntensity;     
    float DiffuseIntensity;     
};
 
struct DirectionalLight  
{
    struct BaseLight Base;      
    vec3 Direction;      
};
 
struct Attenuation
{
    float Constant;      
    float Linear; 
    float Exp;    
};
 
struct PointLight  
{ 
    struct BaseLight Base;
    vec3 Position; 
    Attenuation Atten;    
};
  
uniform int gNumPointLights;
uniform DirectionalLight gDirectionalLight; 
uniform PointLight gPointLights[MAX_POINT_LIGHTS];   
uniform vec3 gEyeWorldPos;
uniform float gMatSpecularIntensity = 0.f;    
uniform float gSpecularPower = 0.f;

uniform sampler2D uTextureGrass;
uniform sampler2D uTextureLantern;
uniform sampler2D uTextureStone;
uniform sampler2D uTextureSky;
uniform sampler2D uTextureTronc;
uniform sampler2D uTextureLeaves;
uniform sampler2D uTextureTerre;
uniform sampler2D uTextureBedrock;
uniform sampler2D uTextureBroke;

uniform sampler2D uTextureText;
uniform bool uSkyBox;
uniform bool uText;
  
vec4 CalcLightInternal(struct BaseLight Light, vec3 LightDirection, vec3 Normal)  
{ 
    vec4 AmbientColor = vec4(Light.Color, 1.0f) * Light.AmbientIntensity;  
    float DiffuseFactor = dot(Normal, -LightDirection);   
  
    vec4 DiffuseColor  = vec4(0, 0, 0, 0);  
    vec4 SpecularColor = vec4(0, 0, 0, 0);  
  
    if (DiffuseFactor > 0) {     
 DiffuseColor = vec4(Light.Color, 1.0f) * Light.DiffuseIntensity * DiffuseFactor; 
  
 vec3 VertexToEye = normalize(gEyeWorldPos - WorldPos0);     
 vec3 LightReflect = normalize(reflect(LightDirection, Normal));    
 float SpecularFactor = dot(VertexToEye, LightReflect);
 SpecularFactor = pow(SpecularFactor, gSpecularPower); 
 if (SpecularFactor > 0) {
     SpecularColor = vec4(Light.Color, 1.0f) * gMatSpecularIntensity * SpecularFactor; 
 }
    }    
  
    return (AmbientColor + DiffuseColor + SpecularColor); 
} 
  
vec4 CalcDirectionalLight(vec3 Normal)  
{ 
    return CalcLightInternal(gDirectionalLight.Base, gDirectionalLight.Direction, Normal);
} 
  
vec4 CalcPointLight(struct PointLight l, vec3 Normal)     
{ 
    vec3 LightDirection = WorldPos0 - l.Position;  
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);    
  
    vec4 Color = CalcLightInternal(l.Base, LightDirection, Normal); 
    float Attenuation =  l.Atten.Constant + 
    l.Atten.Linear * Distance +      
    l.Atten.Exp * Distance * Distance;
  
    return Color / Attenuation;  
} 
  
void main()  
{ 
    vec3 Normal = normalize(Normal0);   
    vec4 TotalLight = CalcDirectionalLight(Normal);     

    for (int i = 0 ; i < gNumPointLights ; i++){
        TotalLight += CalcPointLight(gPointLights[i], Normal);
    }

    if (uSkyBox) fFragColor = texture(uTextureSky,TexCoord0);
    else if(uText){
        fFragColor = texture(uTextureText, TexCoord0);
    } 
    else
    {         
        if(texType==0.0)fFragColor = texture(uTextureGrass, TexCoord0) * TotalLight;
        else if ((texType <1.1) && (texType>0.9)) fFragColor = texture(uTextureTerre, TexCoord0) * TotalLight;
        else if ((texType <2.1) && (texType>1.9)) fFragColor = texture(uTextureStone, TexCoord0)*TotalLight;
        else if ((texType <3.1) && (texType>2.9)) fFragColor = texture(uTextureLantern, TexCoord0)*TotalLight;
        else if ((texType <4.1) && (texType>3.9)) fFragColor = texture(uTextureTronc, TexCoord0)*TotalLight;
        else if ((texType <5.1) && (texType>4.9)) fFragColor = texture(uTextureLeaves, TexCoord0)*TotalLight;
        else if ((texType <6.1) && (texType>5.9)) fFragColor = texture(uTextureBedrock, TexCoord0)*TotalLight; 
        else if ((texType <7.1) && (texType>6.9)) fFragColor = texture(uTextureBroke, TexCoord0)*TotalLight;
    }
}
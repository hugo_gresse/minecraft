#version 330

in vec3 position; //Position 3D du vertex
in vec3 normal_coords; //Coordonnées des normales
in vec2 tex_coords; //coordonnées de texture
in vec3 instance_position;
in float instance_texType;               
                                                     
uniform mat4 uMVPMatrix;                                   
uniform mat4 uMMatrix;
uniform bool uText;
                                                     
out vec2 TexCoord0;                                  
out vec3 Normal0;                                    
out vec3 WorldPos0;
out float texType;                              
                                                     
void main()                                          
{                                                    
    if(uText) {
		gl_Position = (vec4(position, 1.f));
	}
	else gl_Position = uMVPMatrix *vec4(position+instance_position, 1.f);     
    TexCoord0   = tex_coords;                          
    Normal0     = (uMMatrix * vec4(normal_coords, 0.0)).xyz;  
    WorldPos0   = (uMMatrix * vec4(position+instance_position, 1.0)).xyz;
    texType = instance_texType;
}
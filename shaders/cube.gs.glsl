#version 330
 
layout(points) in;
layout(triangle_strip, max_vertices=32) out;
 
in vec4 pos[];
out vec2 tc;

//contient la matrice Model View Projection (elle sera envoyée par l'application au moment du dessin).
uniform mat4 uMVPMatrix = mat4(1.f) ;// la matrice de transformation sera l'identité par défaut

uniform int uTextureType = 0;
 
void main()
{  
	if(uTextureType==1){ //Skybox
		for(int i = 0; i < gl_in.length(); i++)
		{
			//Face devant //skybok : sud
			tc=vec2(1.0, 2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(3./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(3./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face arrière //north
			tc=vec2(2./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(2./4, 1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face gauche /ouest
			tc=vec2(0.,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0., 1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face droite /Est
			tc=vec2(3./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(3./4, 1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(2./4,2./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(2./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face dessus
			tc=vec2(2./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(2./4, 0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,1./3);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1./4,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face dessous
			tc=vec2(0.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(0.0, 1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
		}
	}
	
	else{
		for(int i = 0; i < gl_in.length(); i++)
		{
			tc=vec2(1.0, 1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face arrière
			tc=vec2(0.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(0.0, 0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face gauche
			tc=vec2(1.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0, 0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face droite
			tc=vec2(0.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0.0, 0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face dessus
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0, 1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(0.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
			
			//Face dessus
			tc=vec2(0.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(0.0, 1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(0.5,-0.5,0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,0.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,-0.5,0.0));
			EmitVertex();
			tc=vec2(1.0,1.0);
			gl_Position = uMVPMatrix*(pos[0].xyzw+vec4(-0.5,-0.5,0.5,0.0));
			EmitVertex();
			EndPrimitive();
		}
	}
}

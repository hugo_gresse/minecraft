
#ifndef _IMAC2GL3_MINECRAFT_HPP_
#define _IMAC2GL3_MINECRAFT_HPP_

#include <iostream>
#include <cstdlib>

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <GL/glew.h>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> 


#include "imac2gl3/scene3D/Scene3D.hpp"
#include "imac2gl3/shapes/GLShapeInstance.hpp"
#include "imac2gl3/shapes/MatrixStack.hpp"

#include "imac2gl3/shader_tools.hpp"


#include "imac2gl3/cameras/FreeFlyCamera.hpp"
#include "imac2gl3/text/Text.hpp"
#include "imac2gl3/timer/Timer.hpp"
#include "imac2gl3/input/Input.hpp"
#include "imac2gl3/interface/interfaceAccueil.hpp"
#include "imac2gl3/interface/interfacePause.hpp"
#include "imac2gl3/interface/interfaceJoueur.hpp"
#include "imac2gl3/interface/interfaceMain.hpp"
#include "imac2gl3/sound/Sound.hpp"

#include "imac2gl3/map/Chunk.hpp"
#include "imac2gl3/map/Map.hpp"
#include "imac2gl3/lighting/Lights.hpp"

#define NBR_TEXTURES 9

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


//static const size_t WINDOW_WIDTH = 0, WINDOW_HEIGHT = 0, BYTES_PER_PIXEL = 32;
static const size_t WINDOW_WIDTH = 612, WINDOW_HEIGHT = 612, BYTES_PER_PIXEL = 32;

using namespace std;

namespace imac2gl3 {	     

// Représente le monde
	class Minecraft {

	    public:
	    	//contructeur

	    	Minecraft(){
	    		scene.intialize("map/myMap.map");
	    	}

	    	int run();

	    private:
	    	Scene3D scene;		 
	};
}
#endif

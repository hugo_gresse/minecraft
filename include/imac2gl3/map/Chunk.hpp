#ifndef _IMAC2GL3_CHUNK_HPP_
#define _IMAC2GL3_CHUNK_HPP_

#include <GL/glew.h>
#include <string>
#include <vector>

#include <iostream>
#include <fstream>
#include "imac2gl3/noise/noiseutils.h"

#include "../shapes/Cube.hpp"


using namespace std;

namespace imac2gl3 {	     

// Représente une partie du monde
	class Chunk {

		static const short Version = 1;

	    public:

	    	void save(string fileName);

		    void load(string fileName);

		    bool rechercheCube(glm::vec3 position, int cas, GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition);

		    bool rechercheCube(glm::vec3 positionClick);

	    	void build(utils::NoiseMap& heightMap); 

	    	void reset(GLfloat x, GLfloat y, GLfloat z, string fileName, utils::NoiseMap& heightMap){
	    		position[0]=x;
				position[1]=y;
				position[2]=z;
				nbcube=0;
			  	build(heightMap);
			  	save(fileName);
			  	modify=true;
			  	needToBeSave=false;
			  	newLight = false;
	    	}

	    	//contructeur
			Chunk(GLfloat x, GLfloat y, GLfloat z, string fileName, utils::NoiseMap& heightMap){
				position[0]=x;
				position[1]=y;
				position[2]=z;
				nbcube=0;
			  	build(heightMap);
			  	save(fileName);
			  	modify=true;
			  	needToBeSave=false;
			  	newLight = false;
			}

			Chunk(){
				nbcube=0;
				modify=true;
				needToBeSave=false;
				newLight = false;
			}

			Chunk(string fileName){
				load(fileName);
				//setInstance();
				modify=true;
				needToBeSave=false;
				newLight = false;
			}

			inline glm::vec3 getPosition(){
				return glm::vec3(position[0],position[1],position[2]);
			}

			inline glm::vec3 getInstancePosition(int indice){
				return glm::vec3(instance_positions[indice], instance_positions[indice+1], instance_positions[indice+2]);
			}

			inline void supprimeCube(int indice){
				//Si le cube n'est pas une bedrock ni une stone => destruction
				if((instance_texType[indice/3]!=6.0)&&(instance_texType[indice/3]!=2.0)){
					instance_positions.erase(instance_positions.begin()+indice, instance_positions.begin()+indice+3);
					instance_texType.erase(instance_texType.begin()+(indice/3));
					unsigned int newSize = nbcube-1;
					if(instance_texType.size()!=newSize){
						cout<<"Erreur suppression"<<endl;
					}
					else nbcube-=1;
					modify = true;
				}
				//Si le cube est une stone => destruction en 2 coup...
				else if(instance_texType[indice/3]==2.0){
					instance_texType[indice/3]=7.0;
					modify = true;
				}
		    }

		   	inline void ajoutCube(glm::vec3 positionNouveauCube, GLfloat typeCube){
		   		instance_positions.push_back(positionNouveauCube.x);
		   		instance_positions.push_back(positionNouveauCube.y);
		   		instance_positions.push_back(positionNouveauCube.z);
				instance_texType.push_back(typeCube);
				modify = true;
				nbcube+=1;
		    }

		    bool newLight;
			int nbcube;
			bool modify; //booléen indiquant si l'instance du Chunk doit être recalculer
			bool needToBeSave; //booléen indiquant si l'état du Chunk
			//doit etre sauvegarder à la sortie du programme car des modifications ont été opérées par l'utilisateur 
			GLfloat position[3];
			vector<GLfloat> instance_positions;
			vector<float> instance_texType; // créer un tableau de GLFloat

			 
	};
}
#endif

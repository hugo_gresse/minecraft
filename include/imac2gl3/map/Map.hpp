
#ifndef _IMAC2GL3_MAP_HPP_
#define _IMAC2GL3_MAP_HPP_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <libnoise/noise.h>
#include <ctime>

#include "imac2gl3/map/Chunk.hpp"
#include "imac2gl3/shapes/GLShapeInstance.hpp"
#include "imac2gl3/lighting/Lights.hpp"
#include "imac2gl3/cameras/FreeFlyCamera.hpp"
#include "imac2gl3/noise/noiseutils.h"

using namespace std;

namespace imac2gl3 {	     

// Représente le monde
	class Map {

	    public:
	    	//contructeur
	    	inline void addChunk(Chunk myChunk){
	    		listChunks.push_back(myChunk);
	    		//nbchunk++;

	    	}

	    	inline void addChunkVisible(Chunk* myChunk){
	    		chunksVisibles.push_back(myChunk);
	    	}

	    	void addLight(glm::vec3 cubePosition){
	    		PointLight p;
	    		p.Position = cubePosition;
	    		bool ok = true;
	    		for(unsigned int i = 0; i<pLight.size(); ++i){
	    			if(pLight[i].Position == cubePosition){
	    				ok=false;
	    				cout<<"FU3"<<endl;
	    			}
	    		}
	    		if(ok){
		    		pLight.push_back(p);
					nblight++;
	    		}
	    	}


	    	void suppressLight (glm::vec3 lightPosition){
	    		for(unsigned int i = 0; i<pLight.size(); ++i){
	    			if(pLight[i].Position == lightPosition){
	    				pLight.erase(pLight.begin()+i);
	    				nblight--;
	    			}
	    		}
	    	}

	    	bool rechercheChunk(glm::vec3 positionClick, int cas, GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition); 

	    	bool rechercheChunk(glm::vec3 positionClick);
	    	
	    	void resetInstances();

	    	void draw();
	    	
	    	void build(GLfloat x, GLfloat y, GLfloat z);

	    	void save(string fileName, FreeFlyCamera camera);

	    	FreeFlyCamera load(string fileName);

	    	Map(){
	    		nbchunk=0;
	    		nblight=0;
	    		instancesVisibles.resize(9);
	    	}

	    	int nbchunk;
	    	int nblight;
	    	//Remarque : le 1er de la liste CHunk visible est censé etre celui sur lequel est le joueur	 
	    	vector<GLShapeInstance> instancesVisibles;
	    	vector<Chunk*> chunksVisibles;
			vector<Chunk> listChunks;
			vector<PointLight> pLight;
			vector<string> listFileChunks;

			//vector<string> fileChunks;
			 
	};
}
#endif

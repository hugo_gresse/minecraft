#ifndef _IMAC2GL3_GLSHAPEINSTANCE_HPP_
#define _IMAC2GL3_GLSHAPEINSTANCE_HPP_

#include <GL/glew.h>
#include "Cube.hpp"
#include "Quad.hpp"
#include "Skybox.hpp"
#include "../map/Chunk.hpp"
#include <vector>


#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define TEXCOORDS_LOCATION 2

namespace imac2gl3 {

class GLShapeInstance {
   
   private:
   GLuint VBO;
   GLuint VAO;
   GLsizei vertexCount;
   
   public :
    GLShapeInstance(const Cube & cube);
    GLShapeInstance(const Quad & quad);
    void fillInstance(const Skybox & skybox);
    GLShapeInstance(const Chunk & chunk);

    GLShapeInstance(){
    }

    void fillInstance(const Chunk & chunk);

    GLShapeInstance& operator=(GLShapeInstance const&  InstanceAcopier){
      if(this!=& InstanceAcopier){
        VBO=InstanceAcopier.VBO;
        VAO=InstanceAcopier.VAO;
        vertexCount=InstanceAcopier.vertexCount;
      }
      return *this;
    }

    void draw();
    void draw(const int nbr_cubes);
    ~GLShapeInstance();

};
    
}

#endif


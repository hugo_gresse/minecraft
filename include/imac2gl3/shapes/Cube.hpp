#ifndef _IMAC2GL3_CUBE_HPP_
#define _IMAC2GL3_CUBE_HPP_

#include <GL/glew.h>

#include "common.hpp"

namespace imac2gl3 {	     

// Représente un cube discrétisé centré en (0, 0, 0) (dans son repère local)
	class Cube {
	    public:
	    	void build(); 

	    	//contructeur
			Cube(){
			  	build();
			}

			//attributs
			GLfloat vertices[36*3];
			GLfloat normal_coords[36*3];
			GLfloat tex_coords[36*2];
			 
	};

}

#endif

#ifndef __LIGHTS_H__
#define __LIGHTS_H__

#include <GL/glew.h>
#include <glm/glm.hpp>

namespace imac2gl3 {
    struct BaseLight
    {
        glm::vec3 Color;
        float AmbientIntensity;
        float DiffuseIntensity;

        BaseLight()
        {
            Color = glm::vec3(0.0f, 0.0f, 0.0f);
            AmbientIntensity = 0.0f;
            DiffuseIntensity = 0.0f;
        }
    };

    struct DirectionalLight : public BaseLight
    {        
        glm::vec3 Direction;

        DirectionalLight()
        {
            Direction = glm::vec3(0.0f, 0.0f, 0.0f);
        }
    };

    struct DirectionalLightUniform{
        public:
            DirectionalLightUniform(){};
            void getLocations(const char* uniform, GLuint program);
            GLint ColorLocation, AmbientLocation, DifLocation, DirLocation;
    };

    struct PointLight : public BaseLight
    {
        glm::vec3 Position;

        struct
        {
            float Constant;
            float Linear;
            float Exp;
        } Attenuation;

        PointLight()
        {
            DiffuseIntensity = 1.f;
            Color = glm::vec3(0.9f, 0.9f, 0.5f);
            Position = glm::vec3(0.f,1.f,0.f);
            Attenuation.Constant = 1.0f;
            Attenuation.Linear = 0.1f;
            Attenuation.Exp = 0.0f;
        }
    };

    struct PointLightUniform{
        public:
            PointLightUniform(){};
            void getLocations(const char* uniform, GLuint program);
            GLint ColorLocation, AmbientLocation, DifLocation, PosLocation, ConstLocation, LinLocation, ExpLocation;
    };

    void sendLight(const DirectionalLight& light, const DirectionalLightUniform& lightUniform);
    void sendLight(const PointLight& light, const PointLightUniform& lightUniform);

}
#endif /* __LIGHTS_H__ */
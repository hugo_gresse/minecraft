
#ifndef _IMAC2GL3_SCENE3D_HPP_
#define _IMAC2GL3_SCENE3D_HPP_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

#include "imac2gl3/map/Map.hpp"
#include "imac2gl3/shapes/GLShapeInstance.hpp"
#include "imac2gl3/shapes/Skybox.hpp"
#include "imac2gl3/cameras/FreeFlyCamera.hpp"

using namespace std;

namespace imac2gl3 {	     

// Représente le monde
	class Scene3D {

	    public:
	    	//contructeur

	    	Scene3D(){
	    		dirLight.Color = glm::vec3(1.0f, 1.0f, 1.0f);
			    dirLight.AmbientIntensity = 0.5f;
			    dirLight.DiffuseIntensity = 0.01f;
			    dirLight.Direction = glm::vec3(1.0f, -1.0, 0.0);
				Skybox mySkybox(1000);
				skyInstance.fillInstance(mySkybox);
	    	}

	    	void intialize(string fileName){
	    		myCamera = map.load(fileName);
	    		//myCamera.moveUp(1.5);
	    	}

			glm::mat4 getViewMatrix() const{
				return myCamera.getViewMatrix();
			}
			
			void drawSky(int nb){
				skyInstance.draw(nb);
			}

			void drawMap(){
				map.draw();
			}

			void save(string fileName){
				map.save(fileName, myCamera);
			}

			void resetInstancesMap(){
				map.resetInstances();
			}

			void rotateCameraLeft(float degrees){
				myCamera.rotateLeft(degrees);
			}

			void rotateCameraUp(float degrees){
				myCamera.rotateUp(degrees);
			}


			void moveCameraLeft(float t){
				myCamera.moveLeft(t);
			}
			void moveCameraFront(float t){
				myCamera.moveFront(t);
			}
			void moveCameraUp(float t){
				myCamera.moveUp(t);
			}

			glm::vec3 testMoveCameraLeft(float t){
				return myCamera.testMoveLeft(t);
			}
			glm::vec3 testMoveCameraFront(float t){
				return myCamera.testMoveFront(t);
			}
			glm::vec3 testMoveCameraUp(float t){
				return myCamera.testMoveUp(t);
			}

			inline void resetPositionCamera(float x, float y, float z){
				myCamera.resetPosition(x,y,z);
			}

			inline void setYPositionCamera(float y){
				myCamera.setYPosition(y);
			}
			float getYPositionCamera(){
				return myCamera.getYPosition();
			}


			glm::vec3 getCurseurPosition(){
				return myCamera.getCurseurPosition();
			}

			bool suppressCube(glm::vec3 positionClick, GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition){
				return map.rechercheChunk(positionClick, 0, typeCube, indiceChunk, indiceCube, cubePosition);
			}

			bool addCube(glm::vec3 positionClick, GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition){
				return map.rechercheChunk(positionClick, 1, typeCube, indiceChunk, indiceCube, cubePosition);
			}

			bool rechercheCube(glm::vec3 positionClick,	GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition){
				return map.rechercheChunk(positionClick, 8, typeCube, indiceChunk, indiceCube, cubePosition);
			}

			void addLight(glm::vec3 cubePosition){
				map.addLight(cubePosition);
			}

			void suppressLight(glm::vec3 lightPosition){
				map.suppressLight(lightPosition);
			}
			
	    	Map map; 
	    	FreeFlyCamera myCamera;
	    	DirectionalLight dirLight;
	    	GLShapeInstance skyInstance;
			 
	};
}
#endif

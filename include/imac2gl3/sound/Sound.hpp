#ifndef _IMAC2GL3_SOUND_HPP_
#define _IMAC2GL3_SOUND_HPP_


#include <iostream>
#include <SDL/SDL.h>
#include <fmod/fmod.h>

namespace imac2gl3 {

class Sound {

	private:
		FMOD_SYSTEM *system;
	    FMOD_RESULT resulMusique;

	    //Music
	    FMOD_SOUND *musique;

	    //Son au clic
	    FMOD_SOUND *clicCloth;
	    FMOD_SOUND *clicDirt;
	    FMOD_SOUND *clicStone;
	    FMOD_SOUND *clicWood;
	    FMOD_SOUND *clicLight;
	    FMOD_SOUND *clicFeuille;
	    FMOD_SOUND *clicDelete;



	    FMOD_CHANNEL *channelMusic;
	    FMOD_CHANNEL *channelStep;

	    //son de pas
	    FMOD_SOUND *Step1;
	    FMOD_SOUND *Step2;
	    bool whichStep;


	    int clicLast;
	    int clicNow;
	    

	public:
		Sound();
		~Sound();
		void setMusique();
		void playMusique();

		void setSoundClic();
		void playClic(int typeCube);

		void setClicLast(int);
		void setClicNow(int);

		void setSoundStep();
		void playStep();
};

}

#endif
#ifndef _IMAC2GL3_INPUT_HPP_
#define _IMAC2GL3_INPUT_HPP_

#include <SDL/SDL.h>

namespace imac2gl3 {
	
	class Input {
		protected:
			char key[SDLK_LAST];
			int mousex,mousey;
			int mousexrel,mouseyrel;
			char mousebuttons[8];
			char quit;
		public:
			Input();
			~Input(){}
			void Update();
			inline char& Key(int i){return key[i];}
			inline int MouseX(){return mousex;}
			inline int MouseY(){return mousey;}
			inline int MouseXrel(){return mousexrel;}
			inline int MouseYrel(){return mouseyrel;}
			inline int MouseButton(int i){return mousebuttons[i];}
			inline int Quit(){return quit;}
	};
}

#endif
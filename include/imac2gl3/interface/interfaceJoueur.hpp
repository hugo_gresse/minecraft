#ifndef _IMAC2GL3_INTERFACEJOUEUR_HPP_
#define _IMAC2GL3_INTERFACEJOUEUR_HPP_

#include "imac2gl3/text/Text.hpp"

namespace imac2gl3 {

class InterfaceJoueur {
   
   private:
		//Etat 0 : interface joueur pas affichée, jeu en "pause"
		//Etat 1 : interface joueur affichée avec terre en main
		//Etat 2 : interface joueur affichée avec pierre en main
		//Etat 3 : interface joueur affichée avec lumière en main

		int etat;

		int nbTexture;

		GLenum id_texture;
		GLuint texture[13];

		GLuint textureCurseur;
		Quad Curseur;
		GLShapeInstance instCurseur;

		GLuint textureDirt;
		Quad Dirt;
		GLShapeInstance instDirt;

		GLuint textureTerre;
		Quad Terre;
		GLShapeInstance instTerre;

		GLuint textureStone;
		Quad Stone;
		GLShapeInstance instStone;

		GLuint textureLight;
		Quad Light;
		GLShapeInstance instLight;

		GLuint textureBois;
		Quad Bois;
		GLShapeInstance instBois;

		GLuint textureFeuille;
		Quad Feuille;
		GLShapeInstance instFeuille;




   public:

   		InterfaceJoueur(GLenum id);
   		~InterfaceJoueur(){ };

   		void draw();

   		int getNbText();

		int getEtat();
		void setEtat(int e);

   		void event(SDL_Event &event);
   		
   	
};

}

#endif



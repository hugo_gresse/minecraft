#ifndef _IMAC2GL3_INTERFACEMAIN_HPP_
#define _IMAC2GL3_INTERFACEMAIN_HPP_

#include "imac2gl3/text/Text.hpp"

namespace imac2gl3 {

class InterfaceMain {
   
   private:
		//Etat 0 : dirt off
		//Etat 1 : dirt on
		//Etat 2 : terre off
		//Etat 3 : terre on
		//Etat 4 : stone off
		//Etat 5 : stone on
		//Etat 6 : light off
		//Etat 7 : light on
		//Etat 8 : bois off
		//Etat 9 : bois on
		//Etat 10 : feuille off
		//Etat 11 : feuille on

		int etat;

		int nbTexture;

		GLenum id_texture;
		GLuint texture[12];

		GLuint textureBras;
		Quad Bras;
		GLShapeInstance instBras;


   public:

   		InterfaceMain(GLenum id);
   		~InterfaceMain(){ };

   		void draw();

   		int getNbText();

		int getEtat();
		void setEtat(int e);

   		void event(SDL_Event &event);
   		
   	
};

}

#endif



#ifndef _IMAC2GL3_INTERFACEPAUSE_HPP_
#define _IMAC2GL3_INTERFACEPAUSE_HPP_

#include "imac2gl3/text/Text.hpp"

namespace imac2gl3 {

class InterfacePause {
   
   private:
		//Etat 0 : pause pas affichée, jeu en route
		//Etat 1 : pause affichée
		//Etat 2 : pause save and quit
		int etat;

		int nbTexture;

		GLenum id_texture;
		GLuint texture[5];

		GLuint texturePlay;
		Quad btn_Play;
		GLShapeInstance instBtn_Play;

		GLuint textureQuit;
		Quad btn_Quit;
		GLShapeInstance instBtn_Quit;

		GLuint texturePause;
		Quad BackGr_Pause;
		GLShapeInstance instBackGr_Pause;


   public:

   		InterfacePause(GLenum id);
   		~InterfacePause(){ };

   		void draw();

   		int getNbText();

		int getEtat();
		void setEtat(int e);

   		void event(SDL_Event &event);
   		
   	
};

}

#endif
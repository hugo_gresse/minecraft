#ifndef __FREEFLYCAMERA__
#define __FREEFLYCAMERA__
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace imac2gl3{
		
	class FreeFlyCamera{ 
		private:
			glm::vec3 m_Position;
			float m_fPhi;
			float m_fTheta;
			glm::vec3 m_FrontVector; //toujours parrallèle au plan zx
			glm::vec3 m_LeftVector; //toujours parrallèle au plan zx
			glm::vec3 m_UpVector;
			glm::vec3 m_FrontVector2;
			
			void computeDirectionVectors(){
				m_FrontVector = glm::vec3(
									sin(m_fPhi),
									0,
									cos(m_fPhi)
								);
				m_LeftVector = glm::vec3(
									sin(m_fPhi+(M_PI/2)),
									0,
									cos(m_fPhi+(M_PI/2))
								);

				m_FrontVector2 = glm::vec3(
					cos(m_fTheta)*sin(m_fPhi),
					sin(m_fTheta),
					cos(m_fTheta)*cos(m_fPhi)
				);
				//m_UpVector = glm::cross(m_FrontVector2, m_LeftVector);
				m_UpVector = glm::vec3(0,1,0);
			}
			
		public:
			FreeFlyCamera(){
				m_Position = glm::vec3(0,0,0);
				m_fPhi = M_PI;
				m_fTheta = 0;
				computeDirectionVectors();
			}

			inline void moveLeft(float t){
				m_Position += t*m_LeftVector;
			}
			inline void moveFront(float t){
				m_Position += t*m_FrontVector;
			}
			inline void moveUp(float t){
				m_Position += t*m_UpVector;
			}

			inline glm::vec3 testMoveFront(float t){
				glm:: vec3 nextPosition = m_Position;
				nextPosition += t*m_FrontVector;
				return nextPosition;
			}
			inline glm::vec3 testMoveLeft(float t){
				glm:: vec3 nextPosition = m_Position;
				nextPosition += t*m_LeftVector;
				return nextPosition;
			}
			inline glm::vec3 testMoveUp(float t){
				glm:: vec3 nextPosition = m_Position;
				nextPosition += t*m_UpVector;
				return nextPosition;
			}

			inline void setYPosition(float y){
				m_Position.y=y;
			}
			inline float getYPosition(){
				return m_Position.y;
			}

			inline void resetPosition(float x, float y,float z){
				m_Position = glm::vec3(x,y,z);
				m_fPhi = M_PI;
				m_fTheta = 0;
				computeDirectionVectors();
			}


			inline void rotateLeft(float degrees){
				m_fPhi += glm::radians(degrees);
				computeDirectionVectors();
			}
			inline void rotateUp(float degrees){
				m_fTheta += glm::radians(degrees);
				if((m_fTheta>M_PI/2)||(m_fTheta<-M_PI/2)){
					m_fTheta -= glm::radians(degrees);
				}
				computeDirectionVectors();
			}

			glm::mat4 getViewMatrix() const{
				return glm::lookAt(m_Position, m_Position + m_FrontVector2, m_UpVector);
			}

			glm::vec3 getCurseurPosition(){
				return glm::vec3(
							m_Position.x + 1.5*m_FrontVector2.x, 
							m_Position.y + 1.5*m_FrontVector2.y, 
							m_Position.z + 1.5*m_FrontVector2.z
						);
			}

			void toString() {
				std::cout << "Position : " << m_Position.x << "," << m_Position.y << "," << m_Position.z << std::endl;
				std::cout << "Phi : " << m_fPhi << std::endl;
				std::cout << "Theta : " << m_fTheta << std::endl;
				std::cout << "Front : " << m_FrontVector.x << "," << m_FrontVector.y << "," << m_FrontVector.z << std::endl;
				std::cout << "Left : " << m_LeftVector.x << "," << m_LeftVector.y << "," << m_LeftVector.z << std::endl;
				std::cout << "Up : " << m_UpVector.x << "," << m_UpVector.y << "," << m_UpVector.z << std::endl;
			}
	};
}

#endif

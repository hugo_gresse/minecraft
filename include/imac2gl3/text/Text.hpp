#ifndef _IMAC2GL3_TEXT_HPP_
#define _IMAC2GL3_TEXT_HPP_

#include <SDL/SDL.h>
#include <string>
#include <SDL/SDL_ttf.h>
#include "imac2gl3/shapes/GLShapeInstance.hpp"

namespace imac2gl3 {

	class Text {



		private:
			Quad quadText;
			GLShapeInstance instanceText;

			SDL_Color couleurText;
			TTF_Font *font; // crée une typo
			GLuint fontTexture; // crée une texture pour stocker la typo
			SDL_Surface *surfaceTemp; // on crée une surface
			SDL_Surface *surface; // on crée une surface

			GLenum idTexture; // GL_TEXTURE0 or something more

			std::string texte; //Texte écrit

		public:
			//     A ecrire, taille typo like word, id text, largeur et hauteur
			Text(std::string text, unsigned taille, GLenum id, unsigned r, unsigned g, unsigned b, float x, float y, float width, float height);
			void initTTF();

			void draw();
			void setText(std::string text);
			void setInt(int num);
			GLenum getIdTexture();
			GLuint getTexture();
			~Text();

	};

}
#endif
Bonjour et merci d'utiliser notre projet Imacraft !

INSTALLATION
Afin de compiler au mieux notre programme, vous devez installer plusieures librairies :
- SDL_image
- SDL_ttf 
- libnoise-dev
- fmod

Vous pouvez, pour ce faire, utiliser la commande : sudo apt-get install <nomlibrairie> 

CREATION ET SUPPRESSION DE MAP
La création de map est automatique et aléatoire ! Elle a lieu lors du démarrage du jeu si aucun fichier myMap.map n'a été détecté dans le dossier Map.
Pour regénérer la map, vous avez juste à supprimer ce fichier myMap.map =)
Les heightmap correspondantes sont sauvegardées à chaque génération dans le dossier éponyme.

COMMANDES
Z : avancer
S : reculer
Q : se déplacer sur la gauche
D : se déplacer sur la droite
Espace : sauter
Clic gauche : casser un cube
Clic droit : poser un cube
F1-F6 : choisir cube à poser
Echap : pause

Bon jeu !

L'équipe des Amoureux <3 (Lauriane Anthony, Aurélie Beauprez, Thomas Demenat et Hugo Gresse)
#include <iostream>
#include <cstdlib>

#include <SDL/SDL.h>
#include <GL/glew.h>

#include <glm/glm.hpp>

#include "imac2gl3/minecraft/Minecraft.hpp"



using namespace std;
using namespace imac2gl3;

int main(int argc, char** argv) {

	/********************************************************************
	* INITIALISATION DU PROGRAMME
	********************************************************************/
    // Initialisation de la SDL
    SDL_Init(SDL_INIT_VIDEO);
    
    // Creation de la fenêtre et d'un contexte OpenGL
    //SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BYTES_PER_PIXEL, SDL_OPENGL | SDL_FULLSCREEN);
    SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, BYTES_PER_PIXEL, SDL_OPENGL);

    SDL_WM_SetCaption("imaCraft", NULL);

    //Icone
    SDL_Surface *icone;
    Uint32 colorkey;
    icone = IMG_Load("images/minecraft.png");
    colorkey = SDL_MapRGB(icone->format, 255, 0, 255);
    SDL_SetColorKey(icone, SDL_SRCCOLORKEY, colorkey);
    SDL_WM_SetIcon(icone,NULL);

    // Initialisation de GLEW
    GLenum error;
    if(GLEW_OK != (error = glewInit())) {
        std::cerr << "Impossible d'initialiser GLEW: " << glewGetErrorString(error) << std::endl;
        return EXIT_FAILURE;
    }
    
    //activation du buffer de profondeur
    glEnable(GL_DEPTH_TEST);

    /*********************************************************************
    * LANCEMENT DU MOTEUR DE JEU
    *********************************************************************/
	Minecraft myMinecraft; 
	return myMinecraft.run();
}

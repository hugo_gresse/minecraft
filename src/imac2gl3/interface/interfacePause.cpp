#include "imac2gl3/interface/interfacePause.hpp"
#include "imac2gl3/text/Text.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

namespace imac2gl3 {
	
	InterfacePause::InterfacePause(GLenum id):
			btn_Play(1,0.2, 0, 0.0),  instBtn_Play(btn_Play), 
			btn_Quit(1,0.2, 0, -0.4), instBtn_Quit(btn_Quit), 
			BackGr_Pause(2.1,2.1, 0, 0),  instBackGr_Pause(BackGr_Pause) {

		//Etat interface affiché
		this->setEtat(0);

		nbTexture = 5;
		id_texture = id;


		texturePlay = 0;
		textureQuit = 2;
		texturePause = 4;

		// IMAGE LOGO
		glGenTextures(nbTexture, texture);
		//Chargement des images
		SDL_Surface *image[5];

		image[0] = IMG_Load("images/interface/bt_retour_jeu.png");
		if (!image[0]){
			std::cerr << "Error loading img retour jeu" << std::endl;
		}
		image[1] = IMG_Load("images/interface/bt_retour_jeu_roll.png");
		if (!image[1]){
			std::cerr << "Error loading img retour jeu roll" << std::endl;
		}
		image[2] = IMG_Load("images/interface/bt_save_quit.png");
		if (!image[2]){
			std::cerr << "Error loading img save quit" << std::endl;
		}
		image[3] = IMG_Load("images/interface/bt_save_quit_roll.png");
		if (!image[3]){
			std::cerr << "Error loading img save quit roll" << std::endl;
		}
		image[4] = IMG_Load("images/interface/bg_pause.png");
		if (!image[4]){
			std::cerr << "Error loading img pause" << std::endl;
		}


		for (int i=0; i<nbTexture; ++i){
			glBindTexture(GL_TEXTURE_2D, texture[i]);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[i]->w, image[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image[i]->pixels);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D,0);
		}
		
		//On a plus besoin des images, on libère la mémoire
		SDL_FreeSurface(*image);
		
	}

	void InterfacePause::draw(){
		
			//std::cout << "id text" << id_texture << std::endl;
				glActiveTexture(id_texture+texturePlay);
				glBindTexture(GL_TEXTURE_2D, texture[texturePlay]);
					
					instBtn_Play.draw();

				glActiveTexture(id_texture+textureQuit);
				glBindTexture(GL_TEXTURE_2D, texture[textureQuit]);
					
					instBtn_Quit.draw();

				glActiveTexture(id_texture+texturePause);
				glBindTexture(GL_TEXTURE_2D, texture[texturePause]);

					//transparence
					glEnable(GL_BLEND);
    				glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
						instBackGr_Pause.draw();
					glDisable(GL_BLEND);


				glActiveTexture(id_texture+texturePlay);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureQuit);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+texturePause);
				glBindTexture(GL_TEXTURE_2D,0);

	}

	int InterfacePause::getNbText(){
		return nbTexture;
	}
	void InterfacePause::setEtat(int e){

		etat = e;
		if(etat != 0) {
			//Interface affiché
			SDL_WM_GrabInput( SDL_GRAB_OFF );
			SDL_ShowCursor(SDL_ENABLE);
		}
			
		else {
			SDL_WM_GrabInput( SDL_GRAB_ON );
			SDL_ShowCursor(SDL_DISABLE);

		}
			
	}
	int InterfacePause::getEtat(){
		return etat;
	}

	void InterfacePause::event(SDL_Event &event){
		


        switch(event.type) {

			/* souris clic */
            case SDL_MOUSEBUTTONDOWN:
                
                //std::cout << event.button.x << " " << event.button.y << std::endl;

                if ( event.button.x > 152 && event.button.x < 456 && event.button.y > 278 && event.button.y < 329 ) {
                   this->setEtat(0);
                }
            
				else if ( event.button.x > 152 && event.button.x < 456  && event.button.y > 396 && event.button.y < 457)
                   this->setEtat(2);

            break;
            /* souris bouge */
            case SDL_MOUSEMOTION:

                if ( event.button.x > 152 && event.button.x < 456 && event.button.y > 278 && event.button.y < 329 ) {
              		texturePlay = 1;
              	}
            
				else if ( event.button.x > 152 && event.button.x < 456  && event.button.y > 396 && event.button.y < 457)
                   textureQuit = 3;
               	else {
               		texturePlay = 0;
               		textureQuit = 2;
               	}


            break;

        }
    }

}


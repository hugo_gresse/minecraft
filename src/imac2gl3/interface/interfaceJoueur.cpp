#include "imac2gl3/interface/interfaceJoueur.hpp"
#include "imac2gl3/text/Text.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

namespace imac2gl3 {
	
	InterfaceJoueur::InterfaceJoueur(GLenum id):
			Curseur(0.1,0.1, 0, 0),  instCurseur(Curseur),
			Dirt(0.15,0.15, -0.45, -0.925),  instDirt(Dirt),
			Terre(0.15,0.15, -0.30, -0.925),  instTerre(Terre),
			Stone(0.15,0.15, -0.15, -0.925),  instStone(Stone),
			Light(0.15,0.15, 0, -0.925),  instLight(Light),
			Bois(0.15,0.15, 0.15, -0.925),  instBois(Bois),
			Feuille(0.15,0.15, 0.3, -0.925),  instFeuille(Feuille){

		//Etat interface affiché
		this->setEtat(0);

		nbTexture = 13;
		id_texture = id;


		textureCurseur = 0;
		textureDirt = 2;
		textureStone = 3;
		textureLight = 5;
		textureTerre = 7;
		textureFeuille = 9;
		textureBois = 11;

		// IMAGE LOGO
		glGenTextures(nbTexture, texture);
		//Chargement des images
		SDL_Surface *image[13];

		image[0] = IMG_Load("images/interface/pointeur.png");
		if (!image[0]){
			std::cerr << "Error loading img pointeur" << std::endl;
		}

		image[1] = IMG_Load("images/interface/barre_herbe_off.png");
		if (!image[1]){
			std::cerr << "Error loading img herbe off" << std::endl;
		}
		image[2] = IMG_Load("images/interface/barre_herbe_on.png");
		if (!image[2]){
			std::cerr << "Error loading img herbe on" << std::endl;
		}

		image[3] = IMG_Load("images/interface/barre_stone_off.png");
		if (!image[3]){
			std::cerr << "Error loading img stone off" << std::endl;
		}
		image[4] = IMG_Load("images/interface/barre_stone_on.png");
		if (!image[4]){
			std::cerr << "Error loading img stone on" << std::endl;
		}

		image[5] = IMG_Load("images/interface/barre_light_off.png");
		if (!image[5]){
			std::cerr << "Error loading img light off" << std::endl;
		}
		image[6] = IMG_Load("images/interface/barre_light_on.png");
		if (!image[6]){
			std::cerr << "Error loading img light on" << std::endl;
		}

		image[7] = IMG_Load("images/interface/barre_terre_off.png");
		if (!image[7]){
			std::cerr << "Error loading img terre off" << std::endl;
		}
		image[8] = IMG_Load("images/interface/barre_terre_on.png");
		if (!image[8]){
			std::cerr << "Error loading img terre on" << std::endl;
		}

		image[9] = IMG_Load("images/interface/barre_feuille_off.png");
		if (!image[9]){
			std::cerr << "Error loading img feuille off" << std::endl;
		}
		image[10] = IMG_Load("images/interface/barre_feuille_on.png");
		if (!image[10]){
			std::cerr << "Error loading img feuille on" << std::endl;
		}

		image[11] = IMG_Load("images/interface/barre_bois_off.png");
		if (!image[11]){
			std::cerr << "Error loading img bois off" << std::endl;
		}
		image[12] = IMG_Load("images/interface/barre_bois_on.png");
		if (!image[12]){
			std::cerr << "Error loading img bois on" << std::endl;
		}

		for (int i=0; i<nbTexture; ++i){
			glBindTexture(GL_TEXTURE_2D, texture[i]);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[i]->w, image[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image[i]->pixels);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D,0);
		}
		
		//On a plus besoin des images, on libère la mémoire
		SDL_FreeSurface(*image);
		
	}

	void InterfaceJoueur::draw(){
		

				glActiveTexture(id_texture+textureCurseur);
				glBindTexture(GL_TEXTURE_2D, texture[textureCurseur]);
					glEnable(GL_BLEND);
    				glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR);
						instCurseur.draw();
					glDisable(GL_BLEND);
					

				glActiveTexture(id_texture+textureDirt);
				glBindTexture(GL_TEXTURE_2D, texture[textureDirt]);
					instDirt.draw();

				glActiveTexture(id_texture+textureStone);
				glBindTexture(GL_TEXTURE_2D, texture[textureStone]);
					instStone.draw();

				glActiveTexture(id_texture+textureLight);
				glBindTexture(GL_TEXTURE_2D, texture[textureLight]);
					instLight.draw();

				glActiveTexture(id_texture+textureTerre);
				glBindTexture(GL_TEXTURE_2D, texture[textureTerre]);
					instTerre.draw();

				glActiveTexture(id_texture+textureFeuille);
				glBindTexture(GL_TEXTURE_2D, texture[textureFeuille]);
					instFeuille.draw();

				glActiveTexture(id_texture+textureBois);
				glBindTexture(GL_TEXTURE_2D, texture[textureBois]);
					instBois.draw();


				glActiveTexture(id_texture+textureCurseur);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureDirt);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureStone);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureLight);
				glBindTexture(GL_TEXTURE_2D,0);

				glActiveTexture(id_texture+textureTerre);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureFeuille);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureBois);
				glBindTexture(GL_TEXTURE_2D,0);

	}

	int InterfaceJoueur::getNbText(){
		return nbTexture;
	}
	void InterfaceJoueur::setEtat(int e){

		etat = e;


		switch (etat) {
			case 1:

				// Herbe sélectionné
				textureDirt = 2;
				textureTerre = 7;
				textureStone = 3;
				textureLight = 5;
				textureBois = 11;
				textureFeuille = 9;
			
			break;
			case 2:

				// terre sélectionné
				textureDirt = 1;
				textureTerre = 8;
				textureStone = 3;
				textureLight = 5;
				textureBois = 11;
				textureFeuille = 9;

			break;
			case 3:

				// Stone sélectionné
				textureDirt = 1;
				textureTerre = 7;
				textureStone = 4;
				textureLight = 5;
				textureBois = 11;
				textureFeuille = 9;

			break;
			case 4:

				// Light sélectionné
				textureDirt = 1;
				textureTerre = 7;
				textureStone = 3;
				textureLight = 6;
				textureBois = 11;
				textureFeuille = 9;

			break;
			case 5:

				// Bois sélectionné
				textureDirt = 1;
				textureTerre = 7;
				textureStone = 3;
				textureLight = 5;
				textureBois = 12;
				textureFeuille = 9;

			break;
			case 6:

				// Feuille sélectionné
				textureDirt = 1;
				textureTerre = 7;
				textureStone = 3;
				textureLight = 5;
				textureBois = 11;
				textureFeuille = 10;

			break;

		}

			
	}
	int InterfaceJoueur::getEtat(){
		return etat;
	}

	void InterfaceJoueur::event(SDL_Event &event){

    }

}


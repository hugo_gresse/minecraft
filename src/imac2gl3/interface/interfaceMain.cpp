#include "imac2gl3/interface/interfaceMain.hpp"
#include "imac2gl3/text/Text.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

namespace imac2gl3 {
	
	InterfaceMain::InterfaceMain(GLenum id):
			Bras(0.7,0.7, 0.8, -0.8),  instBras(Bras) {

		//Etat interface affiché
		this->setEtat(0);

		nbTexture = 12;
		id_texture = id;


		textureBras = 0;

		// IMAGE LOGO
		glGenTextures(nbTexture, texture);
		//Chargement des images
		SDL_Surface *image[12];

		image[0] = IMG_Load("images/interface/bras_herbe_off.png");
		if (!image[0]){
			std::cerr << "Error loading img bras dirt off" << std::endl;
		}
		image[1] = IMG_Load("images/interface/bras_herbe_on.png");
		if (!image[1]){
			std::cerr << "Error loading img bras dirt on" << std::endl;
		}

		image[2] = IMG_Load("images/interface/bras_terre_off.png");
		if (!image[2]){
			std::cerr << "Error loading img terre off" << std::endl;
		}
		image[3] = IMG_Load("images/interface/bras_terre_on.png");
		if (!image[3]){
			std::cerr << "Error loading img terre on" << std::endl;
		}


		image[4] = IMG_Load("images/interface/bras_stone_off.png");
		if (!image[4]){
			std::cerr << "Error loading img stone off" << std::endl;
		}
		image[5] = IMG_Load("images/interface/bras_stone_on.png");
		if (!image[5]){
			std::cerr << "Error loading img stone on" << std::endl;
		}

		image[6] = IMG_Load("images/interface/bras_light_off.png");
		if (!image[6]){
			std::cerr << "Error loading img light off" << std::endl;
		}
		image[7] = IMG_Load("images/interface/bras_light_on.png");
		if (!image[7]){
			std::cerr << "Error loading img light on" << std::endl;
		}
	

		image[8] = IMG_Load("images/interface/bras_bois_off.png");
		if (!image[8]){
			std::cerr << "Error loading img bois off" << std::endl;
		}
		image[9] = IMG_Load("images/interface/bras_bois_on.png");
		if (!image[9]){
			std::cerr << "Error loading img bois on" << std::endl;
		}

		image[10] = IMG_Load("images/interface/bras_feuille_off.png");
		if (!image[10]){
			std::cerr << "Error loading img feuille off" << std::endl;
		}
		image[11] = IMG_Load("images/interface/bras_feuille_on.png");
		if (!image[11]){
			std::cerr << "Error loading img feuille on" << std::endl;
		}



		for (int i=0; i<nbTexture; ++i){
			glBindTexture(GL_TEXTURE_2D, texture[i]);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[i]->w, image[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image[i]->pixels);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D,0);
		}
		
		//On a plus besoin des images, on libère la mémoire
		SDL_FreeSurface(*image);
		
	}

	void InterfaceMain::draw(){
		
				
		glEnable(GL_BLEND);
    	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
						
					
			glActiveTexture(id_texture+textureBras);
			glBindTexture(GL_TEXTURE_2D, texture[textureBras]);
				instBras.draw();



			glActiveTexture(id_texture+textureBras);
			glBindTexture(GL_TEXTURE_2D,0);
			
		glDisable(GL_BLEND);
	}

	int InterfaceMain::getNbText(){
		return nbTexture;
	}
	void InterfaceMain::setEtat(int e){

		etat = e;

		switch(etat){
			case 0:
				textureBras = 0;
			break;
			case 1:
				textureBras = 1;
			break;
			case 2:
				textureBras = 2;
			break;
			case 3:
				textureBras = 3;
			break;
			case 4:
				textureBras = 4;
			break;
			case 5:
				textureBras = 5;
			break;
			case 6:
				textureBras = 6;
			break;
			case 7:
				textureBras = 7;
			break;
			case 8:
				textureBras = 8;
			break;
			case 9:
				textureBras = 9;
			break;
			case 10:
				textureBras = 10;
			break;
			case 11:
				textureBras = 11;
			break;

		}


			
	}
	int InterfaceMain::getEtat(){
		return etat;
	}

	void InterfaceMain::event(SDL_Event &event){
		switch(event.type) {

            
            /* souris clic */
            case SDL_MOUSEBUTTONDOWN:
                this->setEtat(this->getEtat()+1);

            break;
            
            case SDL_MOUSEBUTTONUP:
                this->setEtat(this->getEtat()-1);

            break;
        }
   
    }


}


#include "imac2gl3/map/Map.hpp"

namespace imac2gl3 {
	 
	bool Map::rechercheChunk(glm::vec3 positionClick, int cas, GLfloat typeCube, int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition){
		//Recherche parmis les cubes visibles (On considère des cubes de 2*64 en hauteur)
			unsigned int i=0, indice=0;
			bool find=false;
			Chunk myChunk;

            do{           
                 glm::vec3 positionChunk = chunksVisibles[i]->getPosition();
                 if(((positionClick.x<=(positionChunk.x+(15./2)))&&(positionClick.x>=(positionChunk.x-(15./2))))
                    &&(positionClick.y>=(positionChunk.y-(64)))&&                 	
                 	((positionClick.z<=(positionChunk.z+(15./2)))&&(positionClick.z>=(positionChunk.z-(15./2)))))

                 {
                 	find =true;
                 	indice=i;
                    *indiceChunk = i;
                 }
               	i++;

            }while ((i<9)&&(!find));

            if(find){
            	find = chunksVisibles[indice]->rechercheCube(positionClick, cas, typeCube, indiceChunk, indiceCube,cubePosition);
            }
        return find;

	 }


     bool Map::rechercheChunk(glm::vec3 positionClick){
        //Recherche parmis les cubes visibles (On considère des cubes de 2*64 en hauteur)
            unsigned int i=0, indice=0;
            bool find=false;
            Chunk myChunk;

            do{           
                 glm::vec3 positionChunk = chunksVisibles[i]->getPosition();
                 if(((positionClick.x<=(positionChunk.x+(15./2)))&&(positionClick.x>=(positionChunk.x-(15./2))))
                    &&(positionClick.y>=(positionChunk.y-(64)))&&                   
                    ((positionClick.z<=(positionChunk.z+(15./2)))&&(positionClick.z>=(positionChunk.z-(15./2)))))

                 {
                    find =true;
                    indice=i;
                 }
                i++;

            }while ((i<9)&&(!find));

            if(find){
                find = chunksVisibles[indice]->rechercheCube(positionClick);
            }
        return find;

     }


    void Map::resetInstances(){
        for(unsigned i=0; i<9; ++i){
            if(chunksVisibles[i]->modify){
                instancesVisibles[i].fillInstance(*(chunksVisibles[i]));
                chunksVisibles[i]->modify=false;
            }
        }
    }

    void Map::draw(){
        for(unsigned i=0; i<instancesVisibles.size(); ++i){
             instancesVisibles[i].draw(chunksVisibles[i]->nbcube);
        }
    }

    void Map::save(string fileName, FreeFlyCamera camera){ 

        string extend (".chunk");
        int t;

        for(unsigned i=0; i<listChunks.size(); ++i){
            if(listChunks[i].needToBeSave)
                listChunks[i].save(listFileChunks[i]);
        }

         ofstream f (fileName.c_str(), ios::out | ios::binary);

        if(!f.is_open())
            cout << "Impossible d'ouvrir le fichier en écriture !" << endl;

        else
        {

            f.write ((char *)&nbchunk, sizeof(int));
            f.write ((char *)&nblight, sizeof(int));
            f.write ((char *)&camera, sizeof(FreeFlyCamera));

            for(unsigned int i=0; i<pLight.size(); ++i){
                f.write ((char *)&pLight[i], sizeof(PointLight));
            }

            for(unsigned int i=0; i<listFileChunks.size(); ++i){
                t=listFileChunks[i].size()+1;
                f.write((char *)&t,sizeof(int));
                f.write(listFileChunks[i].c_str(), t);
            }
        }

        f.close();
    }

    //création d'un Chunk et de ses 8 voisins;
    void Map::build(GLfloat x, GLfloat y, GLfloat z){
        string extend (".chunk");
        listFileChunks.resize(9);
        Chunk mychunk;

        srand(time(NULL));
        noise::module::Perlin myModule;
        myModule.SetOctaveCount(rand()%2+1);
        myModule.SetFrequency (0.1*(rand()%4)+0.1);
        utils::NoiseMap heightMap;
        utils::NoiseMapBuilderPlane heightMapBuilder;
        heightMapBuilder.SetSourceModule(myModule);
        heightMapBuilder.SetDestNoiseMap(heightMap);
        heightMapBuilder.SetDestSize(15, 15);

        int posX = rand()%100;
        int posY = rand()%100;

        for(unsigned i=0; i<9; ++i){
            string path("chunks/");
            ostringstream number;
            string fileNumber;

            number << i;
            fileNumber= number.str();
            path += fileNumber;
            path += extend;

           
            

             //chunk central 
            if(i==0) {
                heightMapBuilder.SetBounds(-1.0+posX, 1.0+posX, -1.0+posY, 1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x, y, z, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/central.bmp");
                writer.WriteDestFile();
            }
            //chunk gauche
            else if(i==1) {
                heightMapBuilder.SetBounds(-3.0+posX, -1.0+posX, -1.0+posY, 1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x-15, y, z, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/gauche.bmp");
                writer.WriteDestFile();
            }
            //chunk haut gauche
            else if(i==6){
                heightMapBuilder.SetBounds(-3.0+posX, -1.0+posX, 1.0+posY, 3.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x-15, y, z+15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/hautgauche.bmp");
                writer.WriteDestFile();
            }
            //chunk haut
            else if(i==3){
                heightMapBuilder.SetBounds(-1.0+posX, 1.0+posX, 1.0+posY, 3.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x, y, z+15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/haut.bmp");
                writer.WriteDestFile();
            }
            //chunk haut droite
            else if(i==4){
                heightMapBuilder.SetBounds(1.0+posX, 3.0+posX, 1.0+posY, 3.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x+15, y, z+15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/hautdroite.bmp");
                writer.WriteDestFile();
            }
            //chunk droite
            else if(i==5){
                heightMapBuilder.SetBounds(1.0+posX, 3.0+posX, -1.0+posY, 1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x+15, y, z, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/droite.bmp");
                writer.WriteDestFile();
            }
            //chunk bas droite
            else if(i==2){
                heightMapBuilder.SetBounds(1.0+posX, 3.0+posX, -3.0+posY, -1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x+15, y, z-15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/basdroite.bmp");
                writer.WriteDestFile();
            }
            //chunk bas
            else if(i==7){
                heightMapBuilder.SetBounds(-1.0+posX, 1.0+posX, -3.0+posY, -1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x, y, z-15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/bas.bmp");
                writer.WriteDestFile();
            }
            //chunk bas-gauche
            else if(i==8){
                heightMapBuilder.SetBounds(-3.0+posX, -1.0+posX, -3.0+posY, -1.0+posY);
                heightMapBuilder.Build();
                mychunk.reset(x-15, y, z-15, path, heightMap);

                utils::RendererImage renderer;
                utils::Image image;
                renderer.SetSourceNoiseMap(heightMap);
                renderer.SetDestImage(image);
                renderer.Render();

                utils::WriterBMP writer;
                writer.SetSourceImage(image);
                writer.SetDestFilename("heightmap/basgauche.bmp");
                writer.WriteDestFile();
            }

            addChunk(mychunk);
            nbchunk ++;
            
            listFileChunks[i] = path;
        }
    }


    FreeFlyCamera Map::load(string fileName){
        FreeFlyCamera camera;
        Chunk myChunk;
        int t;

        ifstream f (fileName.c_str(), ios::in | ios::binary);

        if(!f.is_open()){
            cout << "Génération d'une nouvelle Map !" << endl;
            //Création d'une nouvelle map 
            //resetPositionCamera(0,1.5,0);
            nbchunk=0;
            nblight=0;
            instancesVisibles.resize(9);
            build(0,0,0);        
            for(unsigned i=0; i<9; ++i){
                addChunkVisible(&listChunks[i]);
            } 

            glm::vec3 nextPosition = glm::vec3(0,1.5,0);
            //Tant qu'il y  a un cube à la position de spawn : 
            while(rechercheChunk(nextPosition))
            {
                nextPosition.y +=1;
            }
            //nextPosition.y +=1;
            camera.resetPosition(0,nextPosition.y,0);


            save("map/myMap.map", camera);

        }

        else
        {
            //Chargement de la map sauvegardée
            f.read ((char *)&nbchunk, sizeof(int));
            f.read ((char *)&nblight, sizeof(int));
            f.read ((char *)&camera, sizeof(FreeFlyCamera));
           
            pLight.resize(nblight);
            for(unsigned int i=0; i<pLight.size();++i){
                f.read ((char *)&pLight[i], sizeof(PointLight));
            }

            listFileChunks.resize(nbchunk);
            for(unsigned int i=0; i<listFileChunks.size(); ++i){
                f.read((char *)&t,sizeof(int));
                char *buf=new char[t];
                f.read(buf,t);
                listFileChunks[i]=string(buf);
                delete []buf;
            }

            f.close();

            for(unsigned i=0; i<listFileChunks.size(); ++i){
                myChunk.load(listFileChunks[i]);
                addChunk(myChunk);
            }

            for(unsigned i=0; i<9; ++i){
                addChunkVisible(&listChunks[i]);
            }
        }
        return camera;
    }

}
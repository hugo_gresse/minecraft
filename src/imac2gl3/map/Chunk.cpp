#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include "imac2gl3/map/Chunk.hpp"

namespace imac2gl3 {
    //case 0 : suppression Case 1 :ajout case 2 : vérification ajout//
     bool Chunk::rechercheCube(glm::vec3 positionClick, int cas, GLfloat typeCube,  int* indiceChunk, int* indiceCube, glm::vec3 &cubePosition){
    //Recherche parmis les cubes visibles (On considère des chunk de 2*64 en hauteur)
        unsigned int i=0;
        bool find=false;
        int indicePositionCube;

        //vector<GLfloat> instance_positions;
        do{           
             glm::vec3 positionCube = getInstancePosition(i);
             if(
                ((positionClick.x<=(positionCube.x+0.5))&&(positionClick.x>=(positionCube.x-0.5)))&&
                ((positionClick.y<=(positionCube.y+0.5))&&(positionClick.y>=(positionCube.y-0.5)))&&
                ((positionClick.z<=(positionCube.z+0.5))&&(positionClick.z>=(positionCube.z-0.5))))
            {
                find =true;
                indicePositionCube=i;
                *indiceCube = i;

             }
            i+=3;

        }while ((i<instance_positions.size())&&(!find));

        if(find){
            if(cas==0){
                //suppresion 
                /*Récupération de la position du cube*/
                cubePosition = getInstancePosition(indicePositionCube);
                supprimeCube(indicePositionCube);
                modify = true;
                needToBeSave = true;
            }
            else if (cas==1){
            }
            else if(cas==8){
            }
            else{
                //Vérification de l'ajout d'un cube OK => Un cube est présent n dessous à l'indice indicePositionCube du tableau
                glm::vec3 positionNouveauCube =getInstancePosition(indicePositionCube);
                //Calcul de la position du nouveau cube par rapport à celui qui est en dessous
                if(cas==2) positionNouveauCube.y+=1;
                //Calcul de la position du nouveau cube par rapport à celui qui est au dessus
                else if(cas==3) positionNouveauCube.y-=1;
                //Calcul de la position du nouveau cube par rapport à celui qui est à gauche
                else if(cas==4) positionNouveauCube.x+=1;
                //Calcul de la position du nouveau cube par rapport à celui qui est à droite
                else if(cas==5) positionNouveauCube.x-=1;
                //Calcul de la position du nouveau cube par rapport à celui qui est devant
                else if(cas==6) positionNouveauCube.z+=1;
                //Calcul de la position du nouveau cube par rapport à celui qui est derrière
                else if(cas==7) positionNouveauCube.z-=1;
                cubePosition = positionNouveauCube;
                ajoutCube(positionNouveauCube, typeCube);
                if(typeCube == 3.0f) newLight = true;
                modify=true;
                needToBeSave = true;
            }
        }
        else{
            if(cas==0){
            }
            else if(cas==1){
                //On vérifie qu'il existe bien un cube en dessous de la position à laquelle
                //on veut ajouter le cube =>permet de récupérer l'indice du cube en dessous, donc sa position => faire +1 en 
                //y pour obtenir les coordonnées du cube à ajouter
                positionClick.y-=1;
                find = rechercheCube(positionClick,2, typeCube, indiceChunk, indiceCube, cubePosition);
                if(!find){
                    //Si il n'y a pas de cube au dessous, vérification s'il y a un cube au dessus
                    positionClick.y+=2;
                    find = rechercheCube(positionClick,3, typeCube, indiceChunk, indiceCube, cubePosition);
                    if(!find){
                        positionClick.y-=1;
                        //Y a-t_il un cube sur le coté gauche?
                        positionClick.x-=1;
                        find = rechercheCube(positionClick,4, typeCube, indiceChunk, indiceCube, cubePosition);
                        if(!find){
                            //Y a-t_il un cube sur le coté droite?
                            positionClick.x+=2;
                            find = rechercheCube(positionClick,5, typeCube, indiceChunk, indiceCube, cubePosition);
                            if(!find){
                                positionClick.x-=1;
                                //Y a-t_il un cube devant? 
                                positionClick.z-=1;
                                find = rechercheCube(positionClick,6, typeCube, indiceChunk, indiceCube, cubePosition);
                                if(!find){
                                    //Y a-t_il un cube derriere? 
                                    positionClick.z+=2;
                                    find = rechercheCube(positionClick,7, typeCube, indiceChunk, indiceCube, cubePosition);
                                    if(!find){
                                    }
                                }
                            }
                        }
                    }
                }
            }            
            else if(cas==8){
            }
            else{
            }
        }

        return find;

     }

  bool Chunk::rechercheCube(glm::vec3 positionClick){
    //Recherche parmis les cubes visibles (On considère des chunk de 2*64 en hauteur)
        unsigned int i=0;
        bool find=false;
        int indicePositionCube;

        //vector<GLfloat> instance_positions;
        do{           
             glm::vec3 positionCube = getInstancePosition(i);
             if(
                ((positionClick.x<=(positionCube.x+0.5))&&(positionClick.x>=(positionCube.x-0.5)))&&
                ((positionClick.y<=(positionCube.y+0.5))&&(positionClick.y>=(positionCube.y-0.5)))&&
                ((positionClick.z<=(positionCube.z+0.5))&&(positionClick.z>=(positionCube.z-0.5))))
            {
                find =true;
                indicePositionCube=i;

             }
            i+=3;

        }while ((i<instance_positions.size())&&(!find));

        return find;

     }



    void Chunk::load(string fileName) {
        ifstream f (fileName.c_str(), ios::in | ios::binary);

        if(!f.is_open())
            cout << "Impossible d'ouvrir le fichier en lecture !" << endl;

        else
        {
            f.read ((char *)&nbcube, sizeof(int));
           
            for(int i=0; i<3; ++i){
                f.read ((char *)&position[i], sizeof(GLfloat));
            }

            instance_positions.resize(3*nbcube);
            instance_texType.resize(nbcube); 

           for(unsigned int i=0; i<instance_positions.size();++i){
                f.read ((char *)&instance_positions[i], sizeof(GLfloat));
            }
            for(unsigned int i=0; i<instance_texType.size();++i){             
                 f.read ((char *)&instance_texType[i], sizeof(GLfloat));
            } 
        }

        f.close();
    }

    void Chunk::save(string fileName) {
        ofstream f (fileName.c_str(), ios::out | ios::binary);

        if(!f.is_open())
            cout << "Impossible d'ouvrir le fichier en écriture !" << endl;

        else
        {

            f.write ((char *)&nbcube, sizeof(int));
            for(int i=0; i<3; ++i){
                f.write ((char *)&position[i], sizeof(GLfloat));
            }
           for(unsigned int i=0; i<instance_positions.size();++i){
                f.write ((char *)&instance_positions[i], sizeof(GLfloat));
        }
            for(unsigned int i=0; i<instance_texType.size();++i){           
                 f.write ((char *)&instance_texType[i], sizeof(GLfloat));
            } 
        }

        f.close();

    }

	void Chunk::build(utils::NoiseMap& heightMap) {
        instance_positions.clear();
        instance_texType.clear();
        for (int x=-7;x<=7;++x){
            for (int z=-7;z<=7;++z){
                int nbCubeVertical= 64+round(heightMap.GetValue(x+7,z+7)*10);
                nbcube+=nbCubeVertical;
                // std::cout<<round(heightMap.GetValue(x+15,z+15)*10)<<std::endl;
                for(int y=0;y<nbCubeVertical;++y)
                {
                    instance_positions.push_back(x+position[0]);
                    instance_positions.push_back(y+position[1]);
                    instance_positions.push_back(z+position[2]);
                    if (y==0)
                        instance_texType.push_back(6.f);
                    else if (y>0 && y<nbCubeVertical-6)
                        instance_texType.push_back(2.f);
                    else if (y>=nbCubeVertical-6 && y<nbCubeVertical-1)
                        instance_texType.push_back(1.f);
                    else
                        instance_texType.push_back(0.f);
                }
            }
        }
	}
}
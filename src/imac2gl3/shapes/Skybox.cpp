#include <cmath>
#include <vector>
#include <iostream>
#include "imac2gl3/shapes/common.hpp"
#include "imac2gl3/shapes/Skybox.hpp"

namespace imac2gl3 {

	void Skybox::build(GLfloat size) {
	    
	    // On créé le tableau de vertex (36 vertex * 8 composantes par vertex = 36*8 GLfloat dans le tableau
	    // rappel : l'axe des Z pointe vers nous
	   
	    GLfloat vertices[36*8] = {
	    
	    		// face en bas
	    		-0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1./4, 1.0, // posx, posy, posz, normalx, normaly, normalz, texcoordx, texcoordy   		
				-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1./4, 2./3,
	    		0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 2./4, 2./3,
	    		
	    		0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 2./4, 2./3, 
	    		0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 2./4, 1., 
	    		-0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,1./4, 1.0,
    		
	    		//face en haut
	    		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1./4, 1./3,
	    		-0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1./4, 0.0,    		
	    		0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 2./4, 0.0,
	    		
				 0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 2./4, 0.0,				 
	    		0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 2./4, 1./3,
	    		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1./4, 1./3,
	    		
	    		//face à gauche
				-0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1./4, 1./3,
	    		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1./4, 2./3,  		
	    		-0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f,  0., 2./3,
	    		
	    		-0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f,  0., 2./3,
	    		-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0., 1./3,
				-0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1./4, 1./3,
	    		
	    		// face à droite
	    		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 3./4, 1./3,	
	    		0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 3./4, 2./3,
	    		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 2./4, 2./3,
	    		
	    		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 2./4, 2./3,
	    		0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 2./4, 1./3,
	    		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 3./4, 1./3,	
	    		
	    		//face sud
				-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0, 1./3,		
	    		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1., 2./3,   		
	    		0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 3./4, 2./3,

				0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 3./4, 2./3,		
	    		0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 3./4, 1./3,
	    		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0, 1./3,
	    		
	    		//face nord
	    		0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 2./4, 1./3,
	    		0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 2./4, 2./3,
	    		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1./4, 2./3,
	    		
	    		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1./4, 2./3,
	    		-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1./4, 1./3,	
	    		0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 2./4, 1./3
	    		
	    };
	    
	    // On remplit chaque vertex du dataPointer avec les valeurs du tableau
	    // -> A chaque ShapeVertex du tableau de vertex (m_pDataPointer) correspond une ligne (8 GLfloat) du tableau vertices
	    for(GLsizei i = 0; i<36*8; i = i + 8){
	    
			m_pDataPointer[i/8].position.x = vertices[i]*size;
			m_pDataPointer[i/8].position.y = vertices[i+1]*size;
			m_pDataPointer[i/8].position.z = vertices[i+2]*size;
		
			m_pDataPointer[i/8].normal.x = vertices[i+3];
			m_pDataPointer[i/8].normal.y = vertices[i+4];
			m_pDataPointer[i/8].normal.z = vertices[i+5];
		
			m_pDataPointer[i/8].texCoords.x = vertices[i+6];
			m_pDataPointer[i/8].texCoords.y = vertices[i+7];
	    }
	}
}


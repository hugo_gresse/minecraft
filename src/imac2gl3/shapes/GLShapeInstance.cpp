#include "imac2gl3/shapes/common.hpp"
#include "imac2gl3/shapes/GLShapeInstance.hpp"
#include <iostream>

namespace imac2gl3 {
  

    void GLShapeInstance::fillInstance(const Skybox & skybox){
	   // Stockage dans un VBO entrelacé:
	glGenBuffers(1, &(this->VBO));
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	
	glBufferData(GL_ARRAY_BUFFER, skybox.getByteSize(), skybox.getDataPointer(),GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	// Construction du VAO associé:
	glGenVertexArrays(1, &(this->VAO));
	glBindVertexArray(this->VAO);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glEnableVertexAttribArray(TEXCOORDS_LOCATION);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	
	glVertexAttribPointer(
		POSITION_LOCATION,
		skybox.getPositionNumComponents(),
		skybox.getDataType(),
		GL_FALSE,
		skybox.getVertexByteSize(),
		skybox.getPositionOffset()
	);
	
	glVertexAttribPointer(
		NORMAL_LOCATION,
		skybox.getNormalNumComponents(),
		skybox.getDataType(),
		GL_FALSE,
		skybox.getVertexByteSize(),
		skybox.getNormalOffset()
	);
	
	glVertexAttribPointer(
		TEXCOORDS_LOCATION,
		skybox.getTexCoordsNumComponents(),
		skybox.getDataType(),
		GL_FALSE,
		skybox.getVertexByteSize(),
		skybox.getTexCoordsOffset()
	);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	
	this->vertexCount=skybox.getVertexCount();
  }

    void GLShapeInstance::fillInstance(const Chunk & chunk){
    Cube cube;
	GLuint offset = 0;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords) + chunk.instance_positions.size()*sizeof(GLfloat) + chunk.instance_texType.size()*sizeof(GLfloat), NULL, GL_STATIC_DRAW);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.vertices), cube.vertices);
	offset += sizeof(cube.vertices);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.normal_coords), cube.normal_coords);
	offset += sizeof(cube.normal_coords);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.tex_coords), cube.tex_coords);
	offset += sizeof(cube.tex_coords);

	/*glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(chunk.instance_positions), chunk.instance_positions);
	offset += sizeof(chunk.instance_positions);*/

	glBufferSubData(GL_ARRAY_BUFFER, offset, chunk.instance_positions.size()*sizeof(GLfloat), &chunk.instance_positions[0]);
	offset += chunk.instance_positions.size()*sizeof(GLfloat);

	/*glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(chunk.instance_texType), chunk.instance_texType);
	offset += sizeof(chunk.instance_texType);*/
	glBufferSubData(GL_ARRAY_BUFFER, offset, chunk.instance_texType.size()*sizeof(GLfloat), &chunk.instance_texType[0]);
	offset += chunk.instance_texType.size()*sizeof(GLfloat);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)sizeof(cube.vertices));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords)));
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords) + chunk.instance_positions.size()*sizeof(GLfloat)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	//Lit les infos d'instance_position à chaque instance
	glVertexAttribDivisor(3, 1);
	//Lit les infos d'instance_texture à chaque instance
	glVertexAttribDivisor(4, 1);

    }
  
  GLShapeInstance::GLShapeInstance(const Chunk & chunk){

  	Cube cube;
	GLuint offset = 0;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords) + chunk.instance_positions.size()*sizeof(GLfloat) + chunk.instance_texType.size()*sizeof(GLfloat), NULL, GL_STATIC_DRAW);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.vertices), cube.vertices);
	offset += sizeof(cube.vertices);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.normal_coords), cube.normal_coords);
	offset += sizeof(cube.normal_coords);
	
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(cube.tex_coords), cube.tex_coords);
	offset += sizeof(cube.tex_coords);

	/*glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(chunk.instance_positions), chunk.instance_positions);
	offset += sizeof(chunk.instance_positions);*/

	glBufferSubData(GL_ARRAY_BUFFER, offset, chunk.instance_positions.size()*sizeof(GLfloat), &chunk.instance_positions[0]);
	offset += chunk.instance_positions.size()*sizeof(GLfloat);

	/*glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(chunk.instance_texType), chunk.instance_texType);
	offset += sizeof(chunk.instance_texType);*/

	glBufferSubData(GL_ARRAY_BUFFER, offset, chunk.instance_texType.size()*sizeof(GLfloat), &chunk.instance_texType[0]);
	offset += chunk.instance_texType.size()*sizeof(GLfloat);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)sizeof(cube.vertices));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords)));
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid *)(sizeof(cube.vertices) + sizeof(cube.normal_coords) + sizeof(cube.tex_coords) + chunk.instance_positions.size()*sizeof(GLfloat)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	//Lit les infos d'instance_position à chaque instance
	glVertexAttribDivisor(3, 1);
	//Lit les infos d'instance_texture à chaque instance
	glVertexAttribDivisor(4, 1);


	////////////////////////////////////////////////////////////////////
  }
  

	GLShapeInstance::GLShapeInstance(const Quad & quad){
		// Stockage dans un VBO entrelacé:
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glBufferData(GL_ARRAY_BUFFER, quad.getByteSize(), quad.getDataPointer(),GL_STATIC_DRAW);
		//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(normal_coords) + sizeof(tex_coords), NULL, GL_STATIC_DRAW);
		

		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// Construction du VAO associé:
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(this->VAO);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glVertexAttribPointer(
			0,
			3,
			GL_FLOAT,
			GL_FALSE,
			quad.getVertexByteSize(),
			quad.getPositionOffset()
		);

		glVertexAttribPointer(
			1,
			3,
			GL_FLOAT,
			GL_FALSE,
			quad.getVertexByteSize(),
			quad.getNormalOffset()
		);

		glVertexAttribPointer(
			2,
			2,
			GL_FLOAT,
			GL_FALSE,
			quad.getVertexByteSize(),
			quad.getTexCoordsOffset()
		);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);


		this->vertexCount=quad.getVertexCount();
	}
	  

	GLShapeInstance::~GLShapeInstance(){
		glDeleteBuffers(1,&(this->VAO));
		glDeleteVertexArrays(1,&(this->VBO));   
	}


	void GLShapeInstance::draw(){
		glBindVertexArray(this->VAO);
		glDrawArrays(GL_TRIANGLES, 0, this->vertexCount);
		glBindVertexArray(0);
	}

   void GLShapeInstance::draw(const int nbr_cubes){
	   glBindVertexArray(VAO);
		glDrawArraysInstanced(GL_TRIANGLES, 0, 36, nbr_cubes);
		glBindVertexArray(0);
   }
}

#include <cmath>
#include <vector>
#include <iostream>
#include "imac2gl3/shapes/common.hpp"
#include "imac2gl3/shapes/Cube.hpp"

namespace imac2gl3 {

	void Cube::build() {

		static const GLfloat init_vertices[36*3] = {
    		// face en bas
    		-0.5f, -0.5f, 0.5f, // posx, posy, posz, normalx, normaly, normalz, texcoordx, texcoordy   		
			-0.5f, -0.5f, -0.5f,
    		0.5f, -0.5f, -0.5f,
    		
    		0.5f, -0.5f, -0.5f,
    		0.5f, -0.5f, 0.5f,
    		-0.5f, -0.5f, 0.5f,
		
    		//face en haut
    		-0.5f, 0.5f, -0.5f,
    		-0.5f, 0.5f, 0.5f,    		
    		0.5f, 0.5f, 0.5f,
    		
			 0.5f, 0.5f, 0.5f,				 
    		0.5f, 0.5f, -0.5f,
    		-0.5f, 0.5f, -0.5f,
    		
    		//face à gauche
			-0.5f, 0.5f, -0.5f,
    		-0.5f, -0.5f, -0.5f, 		
    		-0.5f, -0.5f, 0.5f, 
    		
    		-0.5f, -0.5f, 0.5f,
    		-0.5f, 0.5f, 0.5f,
			-0.5f, 0.5f, -0.5f,
    		
    		// face à droite
    		0.5f, 0.5f, 0.5f,
    		0.5f, -0.5f, 0.5f,
    		0.5f, -0.5f, -0.5f,
    		
    		0.5f, -0.5f, -0.5f,
    		0.5f, 0.5f, -0.5f,
    		0.5f, 0.5f, 0.5f,
    		
    		//face devant
			-0.5f, 0.5f, 0.5f,
    		-0.5f, -0.5f, 0.5f,
    		0.5f, -0.5f, 0.5f,

			0.5f, -0.5f, 0.5f, 
    		0.5f, 0.5f, 0.5f, 
    		-0.5f, 0.5f, 0.5f,
    		
    		//face derrière
    		0.5f, 0.5f, -0.5f,
    		0.5f, -0.5f, -0.5f,
    		-0.5f, -0.5f, -0.5f,
    		
    		-0.5f, -0.5f, -0.5f,
    		-0.5f, 0.5f, -0.5f,
    		0.5f, 0.5f, -0.5f
	    };

	static const GLfloat init_normal_coords[36*3]= {
			// face en bas
    		0.0f, -1.0f, 0.0f, // normalx, normaly, normalz,   		
			0.0f, -1.0f, 0.0f,
    		0.0f, -1.0f, 0.0f,
    		
    		0.0f, -1.0f, 0.0f,
    		0.0f, -1.0f, 0.0f,
    		0.0f, -1.0f, 0.0f,
		
    		//face en haut
    		0.0f, 1.0f, 0.0f,
    		0.0f, 1.0f, 0.0f,    		
    		0.0f, 1.0f, 0.0f,
    		
			0.0f, 1.0f, 0.0f,				 
    		0.0f, 1.0f, 0.0f,
    		0.0f, 1.0f, 0.0f,
    		
    		//face à gauche
			-1.0f, 0.0f, 0.0f,
    		-1.0f, 0.0f, 0.0f,  		
    		-1.0f, 0.0f, 0.0f,
    		
    		-1.0f, 0.0f, 0.0f,
    		-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
    		
    		// face à droite
    		1.0f, 0.0f, 0.0f,	
    		1.0f, 0.0f, 0.0f,
    		1.0f, 0.0f, 0.0f,
    		
    		1.0f, 0.0f, 0.0f,
    		1.0f, 0.0f, 0.0f,
    		1.0f, 0.0f, 0.0f,	
    		
    		//face devant
			0.0f, 0.0f, 1.0f,		
    		0.0f, 0.0f, 1.0f,   		
    		0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 1.0f,		
    		0.0f, 0.0f, 1.0f,
    		0.0f, 0.0f, 1.0f,
    		
    		//face derrière
    		0.0f, 0.0f, -1.0f,
    		0.0f, 0.0f, -1.0f,
    		0.0f, 0.0f, -1.0f,
    		
    		0.0f, 0.0f, -1.0f,
    		0.0f, 0.0f, -1.0f,	
    		0.0f, 0.0f, -1.0f
	};

	static const GLfloat init_tex_coords[36*2] = {
    		// face en bas
    		0.0f, 0.0f, // texcoordx, texcoordy   		
			0.0f, 1.0f,
    		1./3.f, 1.0f,
    		
    		1./3.f, 1.0f,
    		1./3.f, 0.0f,
    		0.0f, 0.0f,
		
    		//face en haut
    		2./3.f, 0.0f,
    		2./3.f, 1.0f,    		
    		1.0f, 1.0f,
    		
			1.0f, 1.0f,				 
    		1.0f, 0.0f,
    		2./3.f, 0.0f,
    		
    		//face à gauche
			1./3.f, 0.0f,
    		1./3.f, 1.0f,  		
    		2./3.f, 1.0f,
    		
    		2./3.f, 1.0f,
    		2./3.f, 0.0f,
			1./3.f, 0.0f,
    		
    		// face à droite
    		1./3.f, 0.0f,	
    		1./3.f, 1.0f,
    		2./3.f, 1.0f,
    		
    		2./3.f, 1.0f,
    		2./3.f, 0.0f,
    		1./3.f, 0.0f,	
    		
    		//face devant
			1./3.f, 0.0f,		
    		1./3.f, 1.0f,   		
    		2./3.f, 1.0f,

			2./3.f, 1.0f,		
    		2./3.f, 0.0f,
    		1./3.f, 0.0f,
    		
    		//face derrière
    		1./3.f, 0.0f,
    		1./3.f, 1.0f,
    		2./3.f, 1.0f,
    		
    		2./3.f, 1.0f,
    		2./3.f, 0.0f,	
    		1./3.f, 0.0f
	    };

		std::copy(init_vertices, init_vertices+(36*3), vertices);
		std::copy(init_normal_coords, init_normal_coords+(36*3), normal_coords);
		std::copy(init_tex_coords, init_tex_coords+(36*2), tex_coords);
	}
}
#include "imac2gl3/shapes/MatrixStack.hpp"

namespace imac2gl3 {
	
  MatrixStack::MatrixStack(){
	//ajout de la matrice identité dans la pile
	this->m_Stack.push(glm::mat4(1.f));
  }
  
  MatrixStack::~MatrixStack(){ 
  }
  
  //Sauvegarde la matrice en tete de pile
   void MatrixStack::push(){
	   //top() renvoie une référence sur la matrice du haut de la pile
	    m_Stack.push(top());
   }
   
  //Retire la derniere matrice ajoutée à la pile 
   void MatrixStack::pop(){
	   m_Stack.pop();
   }
   
   //Multiplie la matrice en tete de la pile par la matrice m
   void MatrixStack::mult(const glm::mat4& m){
	  m_Stack.push(m_Stack.top()*m);
   }
   
  const glm::mat4& MatrixStack::top() const{
	 return m_Stack.top();
	}
	
	void MatrixStack::set(const glm::mat4& m){
		m_Stack.top()=m;
	}
	void MatrixStack::scale(const glm::vec3& s){
		set(glm::scale(m_Stack.top(), s));
	}
	void MatrixStack::translate(const glm::vec3& t){
		set(glm::translate(m_Stack.top(), t));
	}
	
	void MatrixStack::rotate(float degrees, const glm::vec3& r){
		set(glm::rotate(m_Stack.top(),degrees, r));
	}

}


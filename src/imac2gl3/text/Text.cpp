#include <iostream>
#include <string>
#include <sstream>
#include <SDL/SDL.h>
#include <GL/glew.h>
#include <SDL/SDL_ttf.h>

#include "imac2gl3/shapes/GLShapeInstance.hpp"
#include "imac2gl3/text/Text.hpp"



namespace imac2gl3 {

	Text::Text(std::string text, unsigned taille, GLenum id, unsigned r, unsigned g, unsigned b, float x, float y, GLfloat width = 2, GLfloat height = 0.5):quadText(width, height, x, y),  instanceText(quadText){

		//initialisation de la librairy
		Text::initTTF();

		// Initialisation des elements
		idTexture = id;
		
		couleurText.r = r;
		couleurText.g = g;
		couleurText.b = b;
		//couleurText.a = 133;

		font = TTF_OpenFont("fonts/minecraftia.ttf", taille);

		/* Si la police est nulle, il y a eu une erreur */
		if(!font) {
		    fprintf(stderr, "Erreur de création de la police :  : %s\n", TTF_GetError());
			exit(EXIT_FAILURE);
		}
		
		this->setText(text);

		
	}

    void Text::initTTF(){

		if(!TTF_WasInit() && TTF_Init() != 0)
			{
			    fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
			    exit(EXIT_FAILURE);
			}
    }
    
    void Text::draw(){
			//glUniform1i(glGetUniformLocation(program, "uTextureOak"), 1);
    	    
    	    //Activation de la transparence
    		glEnable(GL_BLEND);
    		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    	// glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ONE_MINUS_SRC_COLOR); Pour faire un effet "négatif /positif"
			

			glActiveTexture(idTexture);
			glBindTexture(GL_TEXTURE_2D, fontTexture);
				
				instanceText.draw();

			glActiveTexture(idTexture);
			glBindTexture(GL_TEXTURE_2D,0);

			//Activation de la transparence
		    glDisable(GL_BLEND);
    }
    void Text::setText(std::string text){
    	texte = text;
    	//surfaceTemp = TTF_RenderText_Blended(font, texte.c_str(), couleurText);
    	surfaceTemp = TTF_RenderText_Solid(font, texte.c_str(), couleurText);

		/* Si la surface est nulle, il y a eu une erreur */
		if(!surfaceTemp) {
		    fprintf(stderr, "Erreur de rendu du texte : %s\n", TTF_GetError());
		    exit(EXIT_FAILURE);
		}


		// Transform the sdl text surface into a opengl one
		//std::cout << surfaceTemp->w << " "<< surfaceTemp->h << std::endl;
		unsigned short w = 1, h = 1;
		h = (int) surfaceTemp->h;
		w = (int) surfaceTemp->w;
	    //w = (int) floor(pow(2.0, ceil(log(surfaceTemp->w) / log(2))));
	    //h = (int) floor(pow(2.0, ceil(log(TTF_FontHeight(font)) / log(2))));

	    surface = SDL_CreateRGBSurface(SDL_HWSURFACE, w, h, 32, surfaceTemp->format->Rmask, surfaceTemp->format->Gmask, surfaceTemp->format->Bmask, surfaceTemp->format->Amask);
	    //SDL_SetAlpha(surfaceTemp, 0, 0);
	    SDL_BlitSurface(surfaceTemp, NULL, surface, NULL);
	    

	    //attach the surface to the texture
	    glGenTextures(1, &fontTexture);
		glBindTexture(GL_TEXTURE_2D, fontTexture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);// to eliminate *white edge* effect
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D,0);

		SDL_FreeSurface(surfaceTemp); 
		SDL_FreeSurface(surface); 

    }

    void Text::setInt(int num){

    	// créer un flux de sortie
	    std::ostringstream oss;
	    // écrire un nombre dans le flux
	    oss << num;
	    // récupérer une chaîne de caractères

    	this->setText(oss.str());
    }

    Text::~Text(){
		//Arret de la SDL_TTF et des typo
		glDeleteTextures(1, &fontTexture);
		TTF_CloseFont(font); /* Doit être avant TTF_Quit() */
		TTF_Quit();
    }

    GLenum Text::getIdTexture(){
    	return idTexture;
    }
    GLuint Text::getTexture(){
    	return idTexture;
    }

}



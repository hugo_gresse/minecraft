#include "imac2gl3/lighting/Lights.hpp"
#include <glm/gtc/type_ptr.hpp> 
#include <string>

namespace imac2gl3
{
	 
	void DirectionalLightUniform::getLocations(const char* uniform, GLuint program)
	{
		ColorLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.Color").c_str());
		AmbientLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.AmbientIntensity").c_str());
		DifLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.DiffuseIntensity").c_str());
		DirLocation = glGetUniformLocation(program,(std::string(uniform) + ".Direction").c_str());
	}

	void PointLightUniform::getLocations(const char* uniform, GLuint program)
	{
		ColorLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.Color").c_str());
		AmbientLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.AmbientIntensity").c_str());
		DifLocation = glGetUniformLocation(program,(std::string(uniform) + ".Base.DiffuseIntensity").c_str());
		PosLocation = glGetUniformLocation(program,(std::string(uniform) + ".Position").c_str());
		ConstLocation = glGetUniformLocation(program,(std::string(uniform) + ".Atten.Constant").c_str());
		LinLocation = glGetUniformLocation(program,(std::string(uniform) + ".Atten.Linear").c_str());
		ExpLocation = glGetUniformLocation(program,(std::string(uniform) + ".Atten.Exp").c_str());
	}

	void sendLight(const DirectionalLight& light, const DirectionalLightUniform& lightUniform){
		glUniform3fv(lightUniform.ColorLocation, 1,glm::value_ptr(light.Color));
		glUniform1f(lightUniform.AmbientLocation, light.AmbientIntensity);
		glUniform1f(lightUniform.DifLocation, light.DiffuseIntensity);
		glUniform3fv(lightUniform.DirLocation, 1,glm::value_ptr(light.Direction));
	}

	void sendLight(const PointLight& light, const PointLightUniform& lightUniform){
		glUniform3fv(lightUniform.ColorLocation, 1,glm::value_ptr(light.Color));
		glUniform1f(lightUniform.AmbientLocation, light.AmbientIntensity);
		glUniform1f(lightUniform.DifLocation, light.DiffuseIntensity);
		glUniform3fv(lightUniform.PosLocation, 1,glm::value_ptr(light.Position));
		glUniform1f(lightUniform.ConstLocation, light.Attenuation.Constant);
		glUniform1f(lightUniform.LinLocation, light.Attenuation.Linear);
		glUniform1f(lightUniform.ExpLocation, light.Attenuation.Exp);
	}
}
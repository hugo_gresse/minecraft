#include "imac2gl3/sound/Sound.hpp"

namespace imac2gl3 {

	Sound::Sound(){
		FMOD_System_Create(&system);
    	FMOD_System_Init(system, 2, FMOD_INIT_NORMAL, NULL);

    	this->setMusique();
    	this->setSoundClic();
    	this->setSoundStep();

    	clicLast = 0;
    	clicNow = 0;
    	whichStep = false;
	}
	Sound::~Sound(){
		FMOD_Sound_Release(musique);
		FMOD_Sound_Release(clicCloth);
		FMOD_Sound_Release(clicStone);
		FMOD_Sound_Release(clicWood);
		FMOD_Sound_Release(Step1);
		FMOD_Sound_Release(Step2);
	    FMOD_System_Close(system);
	    FMOD_System_Release(system);

	}
	void Sound::setMusique(){
		
		resulMusique = FMOD_System_CreateSound(system, "sound/music.mp3", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &musique);
		// On vérifie si elle a bien été ouverte (IMPORTANT) 
	    if (resulMusique != FMOD_OK)
	    {
	        std::cerr << "Sound : impossible de lire le fichier mp3 (fichier ? nom ?)" << std::endl;
	        exit(EXIT_FAILURE);
	    }

	    // On active la répétition de la musique à l'infini 
	    FMOD_Sound_SetLoopCount(musique, -1);
	}
	void Sound::setSoundClic(){

		/* Chargement du son et vérification du chargement */
		//0 : Herbe
	    resulMusique = FMOD_System_CreateSound(system, "sound/grass3.ogg", FMOD_CREATESAMPLE, 0, &clicCloth);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire grass3.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	    //1 : Dirt
	    resulMusique = FMOD_System_CreateSound(system, "sound/cloth1.ogg", FMOD_CREATESAMPLE, 0, &clicDirt);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire cloth1.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	    //2 : Stone
	    resulMusique = FMOD_System_CreateSound(system, "sound/stone3.ogg", FMOD_CREATESAMPLE, 0, &clicStone);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire stone3.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	    //3 : Lanterne
	    resulMusique = FMOD_System_CreateSound(system, "sound/wood4.ogg", FMOD_CREATESAMPLE, 0, &clicLight);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire wood4.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	    //4 : Wood (tron)
	    resulMusique = FMOD_System_CreateSound(system, "sound/wood3.ogg", FMOD_CREATESAMPLE, 0, &clicWood);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire wood3.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	    //5 : Feuille
	    resulMusique = FMOD_System_CreateSound(system, "sound/grass2.ogg", FMOD_CREATESAMPLE, 0, &clicFeuille);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire grass2.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }


	    //Delete
	    resulMusique = FMOD_System_CreateSound(system, "sound/wood4.ogg", FMOD_CREATESAMPLE, 0, &clicDelete);
	    if (resulMusique != FMOD_OK){
	       	std::cerr << "Impossible de lire wood4.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }
	}
	void Sound::setSoundStep(){
		/* Chargement du son et vérification du chargement */
	    resulMusique = FMOD_System_CreateSound(system, "sound/step/cloth1.ogg", FMOD_CREATESAMPLE, 0, &Step1);
	    if (resulMusique != FMOD_OK) {
	       	std::cerr << "Impossible de lire step/cloth1.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }

	    resulMusique = FMOD_System_CreateSound(system, "sound/step/cloth2.ogg", FMOD_CREATESAMPLE, 0, &Step2);
	    if (resulMusique != FMOD_OK)
	    {
	       	std::cerr << "Impossible de lire step/cloth2.ogg" << std::endl;
	        exit(EXIT_FAILURE);
	    }

	}

	void Sound::playMusique(){
		/* On joue la musique */
   		FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, musique, 0, &channelMusic);
	}

	void Sound::playClic(int typeCube){
		//init cliclast
		if(clicLast == 0) clicLast = clicNow;
		
		//Vérif son trop rapide
		//if(clicNow - clicLast > 5){
			this->setClicLast(clicNow);
			switch(typeCube){
					case 0:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicCloth, 0, NULL);
					break;
					case 1:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicDirt, 0, NULL);
					break;
					case 2:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicStone, 0, NULL);
					break;
					case 3:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicLight, 0, NULL);
					break;
					case 4:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicWood, 0, NULL);
					break;
					case 5:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicFeuille, 0, NULL);
					break;
					case 1337:
						FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, clicDelete, 0, NULL);
					break;
			}

		//}

		

		//FMOD_Channel_SetPaused(channelMusic, false);
	}

	void Sound::playStep(){
		//init cliclast
		if(clicLast == 0) clicLast = clicNow;

		//Vérif son trop rapide
		if(clicNow - clicLast > 8){
			this->setClicLast(clicNow);
			if(whichStep){
				FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, Step1, true, &channelStep);
				whichStep = false;
			} else {
				FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, Step2, true, &channelStep);
				whichStep = true;
			}

			FMOD_Channel_SetVolume(channelStep, 0.35);
			FMOD_Channel_SetPaused(channelStep, false);
		}
	}


	void Sound::setClicLast(int frame){ clicLast = frame; }
	void Sound::setClicNow(int frame){ clicNow = frame; }

	

	
}
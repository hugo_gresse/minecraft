#include "imac2gl3/minecraft/Minecraft.hpp"

namespace imac2gl3 {

int Minecraft::run(){

    
/**************************************************
* CREATION DES VARIABLES NECESSAIRES
*************************************************/

	//Variables événements souris
	int mouseX, mouseY;
	glm::vec3 positionClick ;     
	
	//Variable gestion de la gravité
	float ymove = 0;
	bool fall = false;
	bool jump = false;
	float jump_high = 0.2;
	float gravity = 0.02;

	GLfloat typeCube = 0.0;

	//Variables de gestion des collisions
	glm::vec3 nextPosition;
	glm::vec3 cubePosition;
	int indiceChunk;
	int indiceCube;

	bool leftclick = false;
	bool rightclick = false;

/********************************************************************
* CREATION DES RESSOURCES OPENGL
********************************************************************/
	//Textures
	if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) == -1)
	{
		std::cerr << "Error IMG init" << std::endl;
		return EXIT_FAILURE;
	}
	
	//Tableau de textures
	GLuint texture[NBR_TEXTURES];
	glGenTextures(NBR_TEXTURES,texture);

	//Chargement des images
	SDL_Surface *image[NBR_TEXTURES];
	image[0] = IMG_Load("images/grass.jpg");
	if (!image[0]){
		std::cerr << "Error loading img grass" << std::endl;
	}
	
	image[1] = IMG_Load("images/stone.jpg");
	if (!image[1]){
		std::cerr << "Error loading img stone" << std::endl;
	}
	image[2] = IMG_Load("images/skybox.jpg");
	if (!image[2]){
		std::cerr << "Error loading img skybox" << std::endl;
	}
	image[3] = IMG_Load("images/lantern.jpg");
	if (!image[3]){
		std::cerr << "Error loading img lantern" << std::endl;
	}
	image[4] = IMG_Load("images/tronc.jpg");
	if (!image[4]){
		std::cerr << "Error loading img lantern" << std::endl;
	}
	image[5] = IMG_Load("images/leaves.jpg");
	if (!image[5]){
		std::cerr << "Error loading img lantern" << std::endl;
	}
	image[6] = IMG_Load("images/terre.jpg");
	if (!image[6]){
		std::cerr << "Error loading img lantern" << std::endl;
	}
	image[7] = IMG_Load("images/bedrock.jpg");
	if (!image[7]){
		std::cerr << "Error loading img lantern" << std::endl;
	}
	image[8] = IMG_Load("images/broke.jpg");
	if (!image[8]){
		std::cerr << "Error loading img lantern" << std::endl;
	}

	//On envoie les textures au Vertex Shader
	for (int i=0; i<NBR_TEXTURES; ++i)
	{
		glBindTexture(GL_TEXTURE_2D, texture[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[i]->w, image[i]->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image[i]->pixels);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);// to eliminate *white edge* effect
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D,0);
	}
	
	//On a plus besoin des images, on libère la mémoire
	SDL_FreeSurface(*image);
	
	//Chargement et compilation des shaders
	GLuint program = imac2gl3::loadProgram("shaders/light.vs.glsl", "shaders/light.fs.glsl");
	if(!program) {
		return EXIT_FAILURE;
	}
	glUseProgram(program);
	
	//On envoie la bonne valeur d'uniform pour la matrice MVP
	GLint MVPLocation = glGetUniformLocation(program, "uMVPMatrix");
	GLint MLocation = glGetUniformLocation(program, "uMMatrix");
	
    imac2gl3::MatrixStack mystack;
    glm::mat4 M = glm::mat4(1.f);
    
    mystack.set(glm::perspective(70.0f, SDL_GetVideoSurface()->w / (float) SDL_GetVideoSurface()->h, 0.1f, 1000.f));

/********************************************************************
* DirectionnalLight
********************************************************************/

	/*imac2gl3::DirectionalLight dirLight;
    dirLight.Color = glm::vec3(1.0f, 1.0f, 1.0f);
    dirLight.AmbientIntensity = 0.5f;
    dirLight.DiffuseIntensity = 0.01f;
    dirLight.Direction = glm::vec3(1.0f, -1.0, 0.0);*/

    /*imac2gl3::PointLight pLight[5];
    for (int i=0;i<5;i++)
    {
    	pLight[i].Position += glm::vec3(i*10.f,0.f,0.f);
    }*/

    imac2gl3::PointLightUniform uPointLight[10];
    char PointLightLoc[16];
    for (int i=0;i<10;i++)
    {
    	snprintf(PointLightLoc,16,"gPointLights[%d]",i);
    	uPointLight[i].getLocations(PointLightLoc,program);
    }
    imac2gl3::DirectionalLightUniform uDirLight;
    uDirLight.getLocations("gDirectionalLight",program);

    glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);


/********************************************************************
* Interface
********************************************************************/
	//Initialisation de la classe input, tous les champs à zéro
	Input in;

	//Gestion FPS
	imac2gl3::Text TexteFPS("FPS not init", 60, NBR_TEXTURES, 255, 255, 255, -0.85, -0.9, 0.2, 0.15); 
	imac2gl3::Timer fps;
	fps.start(); 
	long unsigned frame = 0;


	imac2gl3::InterfacePause pause(NBR_TEXTURES+1);
	imac2gl3::InterfaceAccueil accueil(NBR_TEXTURES+1+5);
	imac2gl3::InterfaceJoueur joueur(NBR_TEXTURES+1+5+5);
	imac2gl3::InterfaceMain main(NBR_TEXTURES+1+5+5+7);

/********************************************************************
* Sound
********************************************************************/
	
	Sound MySound;
	MySound.playMusique();

/********************************************************************
* BOUCLE PRINCIPALE
********************************************************************/
    bool done = false;
    while(!done) {
    	/* Récupération du temps au début de la boucle */
        Uint32 startTime = SDL_GetTicks(); 
        frame++;
        // Nettoyage de la fenêtre
        glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);  
               
        //////////////////////////////////////////////////////////////////
        //Si interface d'accueil affiché
        //////////////////////////////////////////////////////////////////

        if(accueil.getEtat() == 1){
        	//Activation de passage d'info aux shader
        	
			glUniform1i(glGetUniformLocation(program, "uTextureText"), 0);
			glUniform1i(glGetUniformLocation(program, "uText"), true);

				accueil.draw();

			glUniform1i(glGetUniformLocation(program, "uText"), false);
			
			//pour que l'interface s'affiche
			//glUniform1i(glGetUniformLocation(program, "uSkyBox"),true);

        } else if(accueil.getEtat() == 0 || pause.getEtat() == 0) {

	        mystack.push();
	        	glUniform1i(glGetUniformLocation(program,"gNumPointLights"),scene.map.nblight);
		        sendLight(scene.dirLight,uDirLight);
		       
		        for (int i=0;i<scene.map.nblight;i++)
					sendLight(scene.map.pLight[i],uPointLight[i]);
	        	
	        	mystack.push();
		        	mystack.mult(scene.getViewMatrix());

					//Envois des matrices M et MVP au Shader dans les variable uniformes uMMatrix et uMVPMatrix
					glUniformMatrix4fv(MLocation,1, GL_FALSE, glm::value_ptr(M));
					glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, glm::value_ptr(mystack.top()));


					glUniform1i(glGetUniformLocation(program, "uTextureSky"),2);
					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_2D, texture[2]);


					glUniform1i(glGetUniformLocation(program, "uSkyBox"),true);

					glCullFace(GL_BACK);
					scene.drawSky(1);
					glCullFace(GL_FRONT);

					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_2D,0);


	        	mystack.pop();

				mystack.mult(scene.getViewMatrix());
				//Envois des matrices M et MVP au Shader dans les variable uniformes uMMatrix et uMVPMatrix
				glUniformMatrix4fv(MLocation,1, GL_FALSE, glm::value_ptr(M));
				glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, glm::value_ptr(mystack.top()));

				//Activation des texture
				glUniform1i(glGetUniformLocation(program, "uTextureGrass"),0);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, texture[0]);
				
				glUniform1i(glGetUniformLocation(program, "uTextureStone"),1);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, texture[1]);

				glUniform1i(glGetUniformLocation(program, "uTextureLantern"),3);
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, texture[3]);

				glUniform1i(glGetUniformLocation(program, "uTextureTronc"),4);
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, texture[4]);

				glUniform1i(glGetUniformLocation(program, "uTextureLeaves"),5);
				glActiveTexture(GL_TEXTURE5);
				glBindTexture(GL_TEXTURE_2D, texture[5]);

				glUniform1i(glGetUniformLocation(program, "uTextureTerre"),6);
				glActiveTexture(GL_TEXTURE6);
				glBindTexture(GL_TEXTURE_2D, texture[6]);

				glUniform1i(glGetUniformLocation(program, "uTextureBedrock"),7);
				glActiveTexture(GL_TEXTURE7);
				glBindTexture(GL_TEXTURE_2D, texture[7]);

				glUniform1i(glGetUniformLocation(program, "uTextureBroke"),8);
				glActiveTexture(GL_TEXTURE8);
				glBindTexture(GL_TEXTURE_2D, texture[8]);


				glUniform1i(glGetUniformLocation(program, "uSkyBox"),false);
				
				//reset des instances des chunks si ils ont été modifiés
				scene.resetInstancesMap();
				scene.drawMap();
				
				//Désactivation des textures

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D,0);
				//glActiveTexture(GL_TEXTURE3);
				//glBindTexture(GL_TEXTURE_2D,0);
				
	        mystack.pop();       
  

        } //end if etat == 0
       	if(accueil.getEtat() == 2){
        	//on quit
        	done=true;
        }
        if(accueil.getEtat() == 0 && pause.getEtat() == 0) {

	        //AFICHE LES FPS
	        glUniform1i(glGetUniformLocation(program, "uTextureText"), 1);
			glUniform1i(glGetUniformLocation(program, "uText"), true);
				
				main.draw();
				joueur.draw();
				TexteFPS.draw();

			glUniform1i(glGetUniformLocation(program, "uText"), false);
			//pour que l'interface s'affiche
			glUniform1i(glGetUniformLocation(program, "uSkyBox"),true);
        } 
        
         ///////////////////////////////////////////////////////////////////
        // SI PAUSE ///////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////

        if(pause.getEtat() == 1){
        	//Activation de passage d'info aux shader
        	
			glUniform1i(glGetUniformLocation(program, "uTextureText"), 1);
			glUniform1i(glGetUniformLocation(program, "uText"), true);
			
				pause.draw();

			glUniform1i(glGetUniformLocation(program, "uText"), false);
			
			//pour que l'interface s'affiche
			//glUniform1i(glGetUniformLocation(program, "uSkyBox"),true);

        } else if(pause.getEtat() == 2) {
        	//SAUVEGARDER
        	//Sauvegarde des chunks modifiés par l'utilisateur
			scene.save("map/myMap.map");
        	//on quit
        	done=true;
        }

        // Mise à jour de l'affichage
        SDL_GL_SwapBuffers();
        
/********************************************************************
* EVENEMENTS CLAVIER ET SOURIS
********************************************************************/

		SDL_PumpEvents();
		Uint8* keys = SDL_GetKeyState(NULL);
		if(pause.getEtat() == 0 && accueil.getEtat() == 0) {

			//if(keys[SDLK_0]) std::cout << "rofl" << std::endl;

			//Evemenents souris
	       	SDL_GetRelativeMouseState(&mouseX, &mouseY);
	       	scene.rotateCameraLeft(-0.1*mouseX);
			scene.rotateCameraUp(-0.1*mouseY);


			if (keys[SDLK_q]){
				nextPosition = scene.testMoveCameraLeft(0.3);
				//recherche d'un cube à la position future de la camera
				if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
					//son step
					if ((!jump)&&(!fall)){
						MySound.setClicNow(frame);
						MySound.playStep();
					}
					scene.moveCameraLeft(0.1);
					//Recherche de la présence d'un cube en dessous
					nextPosition.y -=1.5;
					if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
						fall=true;
					}
				}
			}
				
			if (keys[SDLK_d]){
				nextPosition = scene.testMoveCameraLeft(-0.3);
				//recherche d'un cube à la position future de la camera
				if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {				
					if ((!jump)&&(!fall)){
						MySound.setClicNow(frame);
						MySound.playStep();
					}
					scene.moveCameraLeft(-0.1);
					//Recherche de la présence d'un cube en dessous
					nextPosition.y -=1.5;
					if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
						fall=true;

					}
				}
			}

			if (keys[SDLK_z]){
				nextPosition = scene.testMoveCameraFront(0.3);
				//recherche d'un cube à la position future de la camera
				if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
					if ((!jump)&&(!fall)){
						MySound.setClicNow(frame);
						MySound.playStep();
					}
					scene.moveCameraFront(0.1);
					//Recherche de la présence d'un cube en dessous
					nextPosition.y -=1.5;
					if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
						fall=true;
					}
				}
			}
				
			if (keys[SDLK_s]){
				nextPosition = scene.testMoveCameraFront(-0.3);
				//recherche d'un cube à la position future de la camera
				if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
					if ((!jump)&&(!fall)){
						MySound.setClicNow(frame);
						MySound.playStep();
					}
					scene.moveCameraFront(-0.1);
					//Recherche de la présence d'un cube en dessous
					nextPosition.y -=1.5;
					if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
						fall=true;
					}
				}
			}

			if (keys[SDLK_SPACE]){
				//Le saut n'est permit que si le joueur n'est pas déjà en train de sauter ou de tomber
				if (!fall && !jump) {
					jump = true;
					ymove = jump_high;	
				}
			}	

			if(keys[SDLK_F1]){
				//herbe
				typeCube = 0.0f;
				joueur.setEtat(1);
				main.setEtat(0);
			}
			if(keys[SDLK_F2]){
				//terre
				typeCube = 1.0f;
				joueur.setEtat(2);
				main.setEtat(2);
			}
			if(keys[SDLK_F3]){
				//stone
				typeCube = 2.0f;
				joueur.setEtat(3);
				main.setEtat(4);
			}
			if(keys[SDLK_F4]){
				//lanterne
				typeCube = 3.0f;
				joueur.setEtat(4);
				main.setEtat(6);
			}
			if(keys[SDLK_F5]){
				//tronc
				typeCube = 4.0f;
				joueur.setEtat(5);
				main.setEtat(8);
			}
			if(keys[SDLK_F6]){
				//feuillage
				typeCube = 5.0f;
				joueur.setEtat(6);
				main.setEtat(10);
			}
			if(keys[SDLK_F7]){
				//bedrock
				typeCube = 6.0f;
				//joueur.setEtat(3);
				//main.setEtat(4);
			}
			
			//Suppression d'un cube
			if(leftclick){
				positionClick = scene.getCurseurPosition();
				
				//Si le cube a bien été supprimé
				if(scene.suppressCube(positionClick, typeCube, &indiceChunk, &indiceCube, cubePosition)){
					MySound.setClicNow(frame);
					MySound.playClic(1337);
					scene.suppressLight(cubePosition);
					//Si le joueur a supprimé un cube sous ses pieds
					if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
						fall = true;
					}
					leftclick=false;
				}
			}
				
			//ajout d'un cube
			if(rightclick){
				positionClick = scene.getCurseurPosition();
				if ((typeCube == 3.0)&&(scene.map.nblight<10))
				{
					//Si le cube a bien été ajouté
					if(scene.addCube(positionClick, typeCube,&indiceChunk, &indiceCube, cubePosition)){
						if(scene.map.chunksVisibles[indiceChunk]->newLight){
							scene.addLight(cubePosition);
							//Sound
							MySound.setClicNow(frame);
							MySound.playClic(3);
						}
						scene.map.chunksVisibles[indiceChunk]->newLight = false;
					}
			
				}
				else if(typeCube != 3.0) {
					//Si le cube a bien été ajouté
					if(scene.addCube(positionClick, typeCube,&indiceChunk, &indiceCube, cubePosition)){
						//Gestion des sons sound
						
						MySound.setClicNow(frame);
				
						if(typeCube>-0.1 && typeCube<0.1)
							MySound.playClic(0);
						else if(typeCube>0.9 && typeCube<1.1)
							MySound.playClic(1);
						else if(typeCube>1.9 && typeCube<2.1)
							MySound.playClic(2);
						else if(typeCube>3.9 && typeCube<4.1)
							MySound.playClic(4);
						else if(typeCube>4.9 && typeCube<5.1)
							MySound.playClic(5);
						else std::cout << "no sound for this cube" << std::endl;

					}
				}
				
				rightclick=false;
			}

		
			//Test de collision Chute
			if(fall || jump)
			{
				//position future de la Camera
				nextPosition = scene.testMoveCameraUp(ymove-gravity);
				//recherche d'un cube à la position future en y de la camera
				//nextPosition.y+=0.2;
				if((ymove<0)){
					nextPosition.y-=1.5;
				if(!scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition)) {
					fall = true;
				}
				else{
					if(jump) jump = false;
					fall = false;
					//remise de la camera au bon niveau au sol
					//scene.setYPositionCamera(scene.map.chunksVisibles[indiceChunk]->instance_positions[indiceCube+1]+1.5);
					scene.setYPositionCamera(scene.map.chunksVisibles[indiceChunk]->instance_positions[indiceCube+1]+1.5);
					//myCamera.setYPosition(nextPosition.y+1.5);
					}
				}
			}


			//Mouvements Chute/Saut
			if (fall || jump) {
					ymove -= gravity;
					if(ymove<-0.5){
					ymove = -0.5;
					}
					scene.moveCameraUp(ymove);
					if(scene.getYPositionCamera()<-100){
						nextPosition = glm::vec3(0,0.5,0);
						//Tant qu'il y  a un cube à la position de respawn : 
						while(scene.rechercheCube(nextPosition, typeCube, &indiceChunk, &indiceCube, cubePosition))
						{
							nextPosition.y +=1;
						}
						nextPosition.y +=1;
						scene.resetPositionCamera(0,nextPosition.y,0);

					}
				//Vitesse de chute limite

			} 

			if (!fall && !jump) {
					ymove = 0;
			}

			if (keys[SDLK_p])
					scene.moveCameraUp(0.1);
			if (keys[SDLK_m])
					scene.moveCameraUp(-0.1);

			if (keys[SDLK_ESCAPE])
					pause.setEtat(1);


		}
        // Boucle de gestion des évenements
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            
        	if(accueil.getEtat() == 1) accueil.event(event);
			if(pause.getEtat() == 1 && accueil.getEtat() == 0)   pause.event(event);
			if(pause.getEtat() == 0 && accueil.getEtat() == 0)   main.event(event);

			//std::cout << event.key.keysym.sym << std::endl;
			// 38, 233, 34
            // Traitement de l'évenement fermeture de fenêtre
            if(event.type == SDL_QUIT) {
                done = true;
                break;
            }
                        // mouse click event
            if(event.type == SDL_MOUSEBUTTONDOWN){
				if(event.button.button==SDL_BUTTON_LEFT){
					leftclick = true;
					mouseX = event.button.x;	
					mouseY = event.button.y;	
				}
				if(event.button.button==SDL_BUTTON_RIGHT)
				{
					rightclick = true;
					mouseX = event.button.x;	
					mouseY = event.button.y;	
				}
			}
        }

         /* Calcul du temps écoulé */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        
        // Si trop peu de temps s'est écoulé, on met en pause le programme 
        // Sinon, on affiche le fps
        TexteFPS.setInt( (float)frame / ( fps.get_ticks() / 1000.f ) ) ;
        if(elapsedTime < FRAMERATE_MILLISECONDS) {
        	SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }

        
    } //end main while
    
/********************************************************************
* LIBERATION DE LA MEMOIRE
********************************************************************/

	//Libération des textures
    glDeleteTextures(NBR_TEXTURES,texture);
   
    atexit(SDL_Quit);
    return EXIT_SUCCESS;
}
}